#!/bin/env python

""" pipeline_assoc_testing_stages_config.py

    -Configuration file to set options specific to each stage/task in pipeline_assoc_testing.py
=============================================================================
"""
import os

import AssocUserInput as I

import WitsgwasSoftware as SW

python = SW.python
plink = SW.plink
plink1 = SW.plink1
perl = SW.perl
R = SW.R



# stageDefaults contains the default options which are applied to each stage (command).
# This section is required for every Rubra pipeline.
# These can be overridden by options defined for individual stages, below.
# Stage options which Rubra will recognise are: 
#  - distributed: a boolean determining whether the task should be submitted to a cluster
#      job scheduling system (True) or run on the system local to Rubra (False). 
#  - walltime: for a distributed PBS job, gives the walltime requested from the job
#      queue system; the maximum allowed runtime. For local jobs has no effect.
#  - memInGB: for a distributed PBS job, gives the memory in Gigabytes requested from the 
#      job queue system. For local jobs has no effect.
#  - queue: for a distributed PBS job, this is the name of the queue to submit the
#      job to. For local jobs has no effect. This is currently a mandatory field for
#      distributed jobs, but can be set to None.
#  - modules: the modules to be loaded before running the task. This is intended for  
#      systems with environment modules installed. Rubra will call module load on each 
#      required module before running the task. Note that defining modules for individual 
#      stages will override (not add to) any modules listed here. This currently only
#      works for distributed jobs.



stageDefaults = {
    'distributed': True,
    'queue': 'WitsLong',
    'walltime': "6:00:00",
    'memInGB': 16,
    'name': None,
    'modules': [
#         python,
#         plink,
#         perl,
#         R,
          'gwaspipe',
    ]
}



# stages should hold the details of each stage which can be called by runStageCheck.
# This section is required for every Rubra pipeline.
# Calling a stage in this way carries out checkpointing and, if desired, batch job
# submission. 
# Each stage must contain a 'command' definition. See stageDefaults above for other 
# allowable options.

if I.sexinfo_available:
	stages = {
		'prune_for_assoc_testing': {
			"command": plink + " --bfile %qced_plink_bfiles --indep-pairwise 50 5 0.2 --out prep_assoc"
			},
		'extract_prunein_SNPs': {
			"command": plink + " --bfile %qced_plink_bfiles --extract %prep_assoc_prunein --make-bed --out %prep_assoc_pruned"
			},
		'do_basic_assoc_test': {
			'command': plink + " --bfile %qced_plink_bfiles --maf %cut_maf --assoc --ci %cut_ci --adjust --out %assoc_basename"
			},
		'plot_manhattan_qq': {
			'command': "Rscript ../../create_manhattan_qqplot_using_qqman.R %path2assoc"
			},
		'generate_clusters_for_cmh': {
			'command': plink + " --bfile %qced_plink_bfiles --cluster --cc --ppc 0.05 --out prep_cmh"
			},
		'do_cmh_adjusted_assoc': {
			'command': plink + " --bfile %qced_plink_bfiles --maf %cut_maf --mh --within %cluster_file --adjust --out %cmh_basename"
			},
		}
        
else:
	stages = {
		'prune_for_assoc_testing': {
			"command": plink + " --noweb --bfile %qced_plink_bfiles --allow-no-sex --indep-pairwise 50 5 0.2 --out prep_assoc"
			},
		'extract_prunein_SNPs': {
			"command": plink + " --noweb --bfile %qced_plink_bfiles --allow-no-sex --extract %prep_assoc_prunein --make-bed --out %prep_assoc_pruned"
			},
		'do_basic_assoc_test': {
			'command': plink + " --noweb --bfile %qced_plink_bfiles --allow-no-sex --maf %cut_maf --assoc --ci %cut_ci --adjust --out %assoc_basename"
			},
		'plot_manhattan_qq': {
			'command': "Rscript ../../create_manhattan_qqplot_using_qqman.R %path2assoc"
			},
		'generate_clusters_for_cmh': {
			'command': plink + " --noweb --bfile %qced_plink_bfiles --allow-no-sex --cluster --cc --ppc 0.05 --out prep_cmh"
			},
		'do_cmh_adjusted_assoc': {
			'command': plink + " --noweb --bfile %qced_plink_bfiles --allow-no-sex --maf %cut_maf --mh --within %cluster_file --adjust --out %cmh_basename"
			},
		}
