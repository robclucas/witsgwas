#!/bin/env python

""" pipeline_assoc_testing.py 

    -pipeline for performing GWAS association analysis in plink (versions: 1.07 and 1.9).
=============================================================================


Authors: Lerato Magosi, Scott Hazelhurst.


This program implements an association workflow for human GWAS analysis using plink binary files.

Association analysis tasks include:
1. Basic plink association test
2. CMH association test - Association analysis, accounting for clusters


Options:
For datasets missing sex info, the sexinfo_available variable in PlinkUserInput should be
set to False e.g. sexinfo_available = False 


Assumptions:
Pipeline assumes the following QC steps have been carried out:

Sample qc checking for: 
1. discordant sex information
2. calculating missingness 
3. heterozygosity scores.
4. relatedness (IBD calculations)

SNP qc calculating:
1. minor allele frequencies 
2. SNP missingness 
3. differential missingness
4. Hardy Weinberg Equilibrium deviations.

Divergent Ancestry:
1. Exclusion of individuals showing divergent ancestry


Task management:
It employs Rubra for sending jobs to a linux cluster via PBS Torque (version 2.5). 
Rubra is a pipeline system for bioinformatics workflows that is built on top
of the Ruffus (http://www.ruffus.org.uk/) Python library (Ruffus version 2.2). 
Rubra adds support for running pipeline stages on a distributed computer cluster 
(https://github.com/bjpop/rubra) and also supports parallel evaluation of independent 
pipeline stages. (Rubra version 0.1.5)

The pipeline is configured by an options file in a python file,
including the actual commands which are run at each stage.


Customization:
Pipeline can be extended in future to include:

1. Permutation testing
2. Logistic regression with covariates from pca
3. Emmax - assoc testing
4. Compute maximum independent set for visualization via Graphitz on Dot

See quickstart_pipeline_supplement and extend_pipeline_supplement for templates

"""

# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)

# witsGWAS banner
from pyfiglet import Figlet


# user defined module imports

import Filemanager as FM

import WitsgwasSoftware as SW

import WitsgwasScripts as SC



# Shorthand access to options defined in pipeline_assoc_config.py
#==========================================

working_files = pipeline_options.working_files
preselected_cutoff = pipeline_options.preselected_cutoff
logDir = pipeline_options.pipeline['logDir']



# Data setup process and input organisation
#==========================================

f = Figlet(font='standard')
print f.renderText('witsGWAS')
print "(C) 2015 Lerato E. Magosi, Scott Hazelhurst"
print "http://magosil86.github.io/witsGWAS/"    
print "witsGWAS v0.1.0 is licensed under the MIT license. See LICENSE.txt"
print "----------------------------------------------------------------"


# create a directory for the current project
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = (os.path.join(SC.witsGWAS_PROJECTS_DIR, working_files['projectname']) + 
	'-assoc-' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '/')

print "Current project directory %s" % SC.CURRENT_PROJECT_DIR


FM.create_dir(SC.CURRENT_PROJECT_DIR)


global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"


# cd into the current project dir.
os.chdir(SC.CURRENT_PROJECT_DIR)


# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir


# create a dir. for storing plots
assoc_plots = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "assoc_plots") + '/') 
FM.create_dir(assoc_plots)



# Preselected user cutoffs
#==========================================

cut_maf = preselected_cutoff['maf_cutoff']

cut_ci = preselected_cutoff['conf_int_cutoff']



# Paths to working and intermediate result files
#==========================================

qced_plink_bfiles = working_files['qced_plink_bfiles']

prep_assoc_pruned = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "prep_assoc_pruned")

assoc_basename = "basic_assoc_maf_" + str(cut_maf) + "_ci_" + str(cut_ci)

assoc_fullname = assoc_basename + ".assoc"

cmh_basename = "cmh_assoc_maf_" + str(cut_maf)

path2assoc = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, assoc_fullname)





# Print project information
#==========================================

print "Starting project %s" % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print


# Pipeline declarations
#==========================================


# create a flagfile to start the pipeline as well as basic association testing
FM.create_emptyfile('prep_assoc.Start')



""" Tasks 1-4: Basic plink association test """

#Task1.0: qced_plink_bfiles  -> prep_assoc.prune.in

@transform('prep_assoc.Start',                                      # Input             = prep_assoc
           suffix('.Start'),                                        # Input suffix      = .Start
           '.PruneForAssocTesting.Success')                         # Output suffix     = .PruneForAssocTesting.Success
           
def prune_for_assoc_testing(inputs, outputs):
    """
    Prune dataset to remain with a single SNP per LD block in preparation for 
    association analysis. Prune such that no pair of SNPs within a 50-SNP sliding window 
    (advanced by 5 SNPs each time) has an r_squared value greater than 0.2
    """
    
    flagFile = outputs
    print "Prune dataset to remain with a single SNP per LD block in preparation for association analysis."
    runStageCheck('prune_for_assoc_testing', flagFile, qced_plink_bfiles)
    print "Task 1.0 completed successfully \n"
    
    
#Task2.0: prep_assoc.prune.in  -> prep_assoc_pruned.{fam, bim, bed}

@transform(prune_for_assoc_testing,                                 # Input             = prep_assoc
           suffix('.PruneForAssocTesting.Success'),                 # Input suffix      = .PruneForAssocTesting.Success
           add_inputs("prep_assoc.prune.in"),                       # additional Inputs = prep_assoc.prune.in
           '.ExtractPruneInSNPs.Success')                           # Output suffix     = .ExtractPruneInSNPs.Success
           
def extract_prunein_SNPs(inputs, outputs):
    """
    Extract SNPs to be kept in the analysis using prep_assoc.prune.in.
    """
    
    prep_assoc_prunein = inputs[1]
    
    flagFile = outputs
    print "Extracting SNPs to be kept in the analysis using prep_assoc.prune.in."
    runStageCheck('extract_prunein_SNPs', flagFile, qced_plink_bfiles, prep_assoc_prunein, prep_assoc_pruned)
    print "Task 2.0 completed successfully \n"
    
    
#Task3.0: prep_assoc_pruned.{fam, bim, bed}  -> basic_assoc_maf_0-01_ci_0-95.assoc

@transform(extract_prunein_SNPs,                                    # Input             = prep_assoc
           suffix('.ExtractPruneInSNPs.Success'),                   # Input suffix      = .ExtractPruneInSNPs.Success
           '.doBasicAssocTest.Success')                             # Output suffix     = .doBasicAssocTest.Success
           
def do_basic_assoc_test(inputs, outputs):
    """
    Conduct a basic case/control association test.
    """
    
    flagFile = outputs
    print "Conduct a basic case/control association test."
    runStageCheck('do_basic_assoc_test', flagFile, qced_plink_bfiles, cut_maf, cut_ci, assoc_basename)
    print "Task 3.0 completed successfully \n"
    
    
#Task4: basic_assoc_maf_0-01_ci_0-95.assoc ->  man_plot_unadj_basic_assoc.pdf and qq_plot_unadj_basic_assoc.pdf

@transform(do_basic_assoc_test,                                     # Input             = prep_assoc
           suffix('.doBasicAssocTest.Success'),                     # Input suffix      = .doBasicAssocTest.Success
           '.PlotManhattanQQ.Success')                              # Output suffix     = .PlotManhattanQQ.Success
           
def plot_manhattan_qq(inputs, outputs):
    """
    Generate manhattan and qq plots to visualize the basic association results.
    """
        
    flagFile = outputs
    print "Generating manhattan and qq plots to visualize the basic association results."
    runStageCheck('plot_manhattan_qq', flagFile, path2assoc)
    print "Task 4.0 completed successfully \n"
    



""" Tasks 5-6: CMH association test - Association analysis, accounting for clusters """
    
#Task5: qced_plink_bfiles ->  prep_assoc.cluster

@transform(plot_manhattan_qq,                                       # Input             = prep_assoc
           suffix('.PlotManhattanQQ.Success'),                      # Input suffix      = .PlotManhattanQQ.Success
           '.GenerateClustersForCMH.Success')                       # Output suffix     = .GenerateClustersForCMH.Success
           
def generate_clusters_for_cmh(inputs, outputs):
    """
    Generate cluster files in preparation for carrying out the Cochran-Mantel-Haenszel 
    (CMH) association test. The CMH association test  is used to correct for potential 
    confounding of population stratification. The CMH association test allows for 
    comparison of cases and controls while controlling for clusters within the data. The 
    cc flag will be used to have at least one case and one control in each cluster.
    """
    
    flagFile = outputs
    print ("Generating cluster files in preparation for carrying out a Cochran-Mantel-Haenszel " 
    "(CMH) association test.")
    runStageCheck('generate_clusters_for_cmh', flagFile, qced_plink_bfiles)
    print "Task 5.0 completed successfully \n"
    
    
#Task6: qced_plink_bfiles and prep_assoc.cluster2 -> cmh_assoc_maf_0-01.cmh.adjusted

@transform(generate_clusters_for_cmh,                               # Input             = prep_assoc
           suffix('.GenerateClustersForCMH.Success'),               # Input suffix      = .GenerateClustersForCMH.Success
           add_inputs("prep_cmh.cluster2"),                         # additional Inputs = prep_cmh.cluster2
           '.doAdjustedAssocCMH.Success')                           # Output suffix     = doAdjustedAssocCMH.Success
           
def do_cmh_adjusted_assoc(inputs, outputs):
    """
    After having performed the above matching based on genome-wide IBS, we can now perform 
    the association test conditional on the matching. For this matched analysis, we shall 
    use the Cochran-Mantel-Haenszel (CMH) association statistic, which tests for 
    SNP-disease association conditional on the clustering supplied by the cluster file.
    """
    
    cluster_file = inputs[1]
    
    flagFile = outputs
    print ("Performing the Cochran-Mantel-Haenszel association test conditional on the "
    "clustering in the prep_cmh.cluster2") 
    runStageCheck('do_cmh_adjusted_assoc', flagFile, qced_plink_bfiles, cut_maf, cluster_file, cmh_basename)
    print "Task 6.0 completed successfully \n"



 

