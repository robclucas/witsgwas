#!/bin/env python

'''Calculates the mean of contrast-qc values in celfiles.genoqcfiltered'''

import csv
import Filemanager as FM

def calc_mean_CQC(genoqcresults_file, col_number, mean_cqc_file):
	with open(mean_cqc_file, "w") as outfile:
		meanCQC = FM.find_column_mean(genoqcresults_file, col_number)
		print meanCQC
		if meanCQC < 1.7:
		    warning_statement = "Warning: The mean contrast-qc is less than the recommended Affymetrix threshold (> 1.7)"
		    mean_CQC_statement = "The mean contrast-qc is: "
		    outfile.write(mean_CQC_statement + str(meanCQC) + '\n' + warning_statement)	
		else:
			outfile.write("contrast-QC mean is: " + str(meanCQC))


#----------------------------------------------------------------------    
    
    
if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Test code for meanCQC.py', version='%(prog)s 1.0')

    parser.add_argument('-f', '--genoqcresults', action='store', dest='path2genoqcresults',
					help='Stores the path to the genoqcresult file ')
			
    parser.add_argument('-cn', '--colnumber', action='store', dest='colnumber',
                    type=int,
					help='Stores the colnumber on which the mean will be calculated ')
				
    parser.add_argument('-o', '--meancqc', action='store', dest='path2meancqc',
					help='Stores the path to the meancqc file')
										
	
    arguments = parser.parse_args()

    
    calc_mean_CQC(arguments.path2genoqcresults, arguments.colnumber, arguments.path2meancqc)
