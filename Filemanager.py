import os
import sys
import time
import csv

#----------------------------------------------------------------------
def create_dir(path):
    """ checks if dir: exists, if not, creates dir:  """
    
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir) 
        
#----------------------------------------------------------------------                                
def create_emptyfile(path):
    """ checks if file: exists, if not, creates an empty file:  """
    filepath = path
    if not os.path.isfile(filepath):
        with open(filepath, "w") as outfile:
	    outfile.write('')

#----------------------------------------------------------------------	    
def line_prepender(filepath, line):
    """ Prepend line to beginning of a file """
    
    with open(filepath, 'r+') as outfile:
        content = outfile.read()
        outfile.seek(0, 0) # move file pointer to beginning of file
        outfile.write(line.rstrip('\r\n') + '\n' + content)
        
        
#----------------------------------------------------------------------
def remove_file_dir(path):
    """
    Remove the file or directory
    
    source: Mike Driscoll
    http://www.blog.pythonlibrary.org/2013/11/14/python-101-how-to-write-a-cleanup-script/
    """
    if os.path.isdir(path):
        try:
            os.rmdir(path)
        except OSError:
            print "Unable to remove folder: %s" % path
    else:
        try:
            if os.path.exists(path):
                os.remove(path)
        except OSError:
            print "Unable to remove file: %s" % path
 
#----------------------------------------------------------------------
def cleanup_old_files(path, number_of_days):
    """
    Removes files from the passed in path that are older than or equal 
    to the number_of_days
    
    source: Mike Driscoll
    http://www.blog.pythonlibrary.org/2013/11/14/python-101-how-to-write-a-cleanup-script/
    """
    
    dir = os.path.dirname(path)
    
    print dir
    
    time_in_secs = time.time() - (int(number_of_days) * 24 * 60 * 60)
    
    print time_in_secs
    for root, dirs, files in os.walk(dir, topdown=False):
        print files
        for file_ in files:
            full_path = os.path.join(root, file_)
            print full_path
            stat = os.stat(full_path)
            print stat
 
            if stat.st_mtime <= time_in_secs:
                print "File: %s was last modified %s to be deleted" % (file_, time.localtime(stat.st_mtime))
                remove_file_dir(full_path)
 
        if not os.listdir(root):
            remove_file_dir(root)

#----------------------------------------------------------------------
def find_column_mean(file, column_number):
    """function: Takes a text or csv file as input and calculates the mean
     of a selected column"""
    
    sum = 0
    count = 0
    with open(file, "rb") as infile:
        # note: csv.reader can be used for both csv and text files
		in_txt = csv.reader(infile, delimiter = '\t')
		
		# assumes that the first line in the file is a commented line
		general_header = next(in_txt)
		print general_header
		
		# assumes that the second line in the file contains the column headers
		column_headers = next(in_txt)
		print column_headers
		
		# loops through the lines in the file adding up the entries in the field 
		# specified by column_number.
		for line in in_txt:
			sum += float(line[column_number])
			count += 1
		average = sum/count
		
		print "Number of entries in the chosen field is :", count 
		print "sum is: ", sum
		print "average is: ", average
		
		return average

#----------------------------------------------------------------------

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Test code for Filemanager.py', version='%(prog)s 1.0')

    parser.add_argument('-d', '--dir', action='store', dest='path2dir',
					help='Stores the path to the dir: of interest ')
					
    parser.add_argument('-f', '--file', action='store', dest='path2files',
					help='Stores the path to the files of interest ')
			
    parser.add_argument('-l', '--line', action='store', dest='line2prepend',
					help='Stores the line to be prepended to the file of interest ')
				
    parser.add_argument('-nd', '--numberofdays', action='store', dest='numdays',
					type=float,
					help='Number of days to remove files created before or on that date')
					
    parser.add_argument('-m', '--colmean', action='store', dest='colmean',
					type=int,
					help='Stores the number specifying the field of interest')
					
	
    arguments = parser.parse_args()

    #print 'path2dir     =', arguments.path2dir
    #print sys.argv[1:]
    
    if '-d' in sys.argv:
        create_dir(arguments.path2dir)
        #remove_file_dir(arguments.path2dir)
        
        if '-nd' in sys.argv:
            cleanup_old_files(arguments.path2dir, arguments.numdays)
         
    elif '-f' in sys.argv:
        create_emptyfile(arguments.path2files)
        
        if '-l' in sys.argv:
            line_prepender(arguments.path2files, arguments.line2prepend)
            
        elif '-m' in sys.argv:
            find_column_mean(arguments.path2files, arguments.colmean)
        
            
