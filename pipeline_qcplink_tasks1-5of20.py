#!/bin/env python

""" pipeline_qcplink.py 

    -pipeline for Sample and SNP qc using plink (version 1.9).
=============================================================================

Authors: Lerato Magosi, Scott Hazelhurst.

This program implements a qc workflow for human GWAS analysis using plink binary files.

Sample qc tasks include checking for: 
1. discordant sex information
2. calculating missingness 
3. heterozygosity scores.

SNP qc tasks include calculating:
1. minor allele frequencies 
2. SNP missingness 
3. differential missingness
4. Hardy Weinberg Equilibrium deviations.

Assumptions:
Case -control status has been specified in the .fam file (phenotype info can be added
using the --make-pheno flag in plink1.9)

Options:
For datasets missing sex info, the sexinfo_available variable in PlinkUserInput should be
set to False e.g. sexinfo_available = False 


Task management:
It employs Rubra for sending jobs to a linux cluster via PBS Torque (version 2.5). 
Rubra is a pipeline system for bioinformatics workflows that is built on top
of the Ruffus (http://www.ruffus.org.uk/) Python library (Ruffus version 2.2). 
Rubra adds support for running pipeline stages on a distributed computer cluster 
(https://github.com/bjpop/rubra) and also supports parallel evaluation of independent 
pipeline stages. (Rubra version 0.1.5)

The pipeline is configured by an options file in a python file,
including the actual commands which are run at each stage.

User interaction:
To facilitate user interaction the pipeline tasks has been grouped into smaller sub-pipelines.
pipeline_qcplink_tasks1-5of20.py
pipeline_qcplink_tasks6-14of20.py
pipeline_qcplink_tasks15-20of20.py

Reference:
Anderson, C. et al. Data quality control in genetic case-control association studies. 
Nature Protocols. 5, 1564-1573, 2010

"""

# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)

# witsGWAS banner
from pyfiglet import Figlet

# user defined module imports
import pipeline_qcplink_config as conf

import Filemanager as FM

import WitsgwasSoftware as SW

import WitsgwasScripts as SC


# Shorthand access to options defined in pipeline_qcplink_config.py
#==========================================

working_files = pipeline_options.working_files
logDir = pipeline_options.pipeline['logDir']



# Data setup process and input organisation
#==========================================

f = Figlet(font='standard')
print f.renderText('witsGWAS')
print "(C) 2015 Lerato E. Magosi, Scott Hazelhurst"
print "http://magosil86.github.io/witsGWAS/"    
print "witsGWAS v0.1.0 is licensed under the MIT license. See LICENSE.txt"
print "----------------------------------------------------------------"

# create a directory for the current project
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = (os.path.join(SC.witsGWAS_PROJECTS_DIR, working_files['projectname']) + 
	'-qcplink-' + working_files['projectauthor'] + '-' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '/')

#SC.CURRENT_PROJECT_DIR = ('/home/lerato/witsGWAS/projects/h3data-2015-08-10_13-11-32/') 
print "Current project directory %s" % SC.CURRENT_PROJECT_DIR

FM.create_dir(SC.CURRENT_PROJECT_DIR)


global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"


# cd into the current project dir.
os.chdir(SC.CURRENT_PROJECT_DIR)

# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir

 
# create a dir. for storing plots
qcplink_plots = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "qcplink_plots") + '/') 
FM.create_dir(qcplink_plots)


# Paths to intermediate result files
#==========================================

orig_plink_bfiles = working_files['plink_bfiles']

qcplink_bfiles = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "qcplink")




# Optional tasks
#==========================================




# Print project information
#==========================================

print "Starting project %s" % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print



# Pipeline declarations
#==========================================


# Phase 1 - Sample QC.

# create a flagfile to start the pipeline
FM.create_emptyfile('qcplink.Start')



""" Tasks 1.0 - 1.3: Identify and remove duplicate markers """

#Task1.0: orig_plink_bfiles -> qcplink_check_duplicate_markers.log

@transform('qcplink.Start',                                         # Input             = qcplink
           suffix('.Start'),                                        # Input suffix      = .Start
           '.CheckDuplicateMarkers.Success')                        # Output suffix     = .CheckDuplicateMarkers.Success 
 
def checkfor_duplicate_markers(inputs, outputs):
    """
    Read in plink binary files to check for duplicate markers.
    """
    
    flagFile = outputs
    print "checking for duplicate markers."
    print "results will be written to qcplink_check_duplicate_markers.log"
    runStageCheck('checkfor_duplicate_markers', flagFile, orig_plink_bfiles)
    print "Task 1.0 completed successfully \n"
    
    
#Task1.1: qcplink_check_duplicate_markers.log -> duplicates.txt

@transform(checkfor_duplicate_markers,                                     # Input             = qcplink
           suffix('.CheckDuplicateMarkers.Success'),                       # Input suffix      = CheckDuplicateMarkers.Success
           add_inputs("qcplink_check_duplicate_markers.log"),              # additional Inputs = qcplink_check_duplicate_markers.log
           '.WriteOutDuplicates.Success')                                  # Output suffix     = .WriteOutDuplicates.Success
           
def write_out_duplicate_markers(inputs, outputs):
    """
    Write out duplicate markers to duplicates.txt.
    """
    
    plink_log_duplicate_markers = inputs[1]
    
    flagFile = outputs
    print "Writing out duplicate markers to duplicates.txt."
    runStageCheck('write_out_duplicate_markers', flagFile, plink_log_duplicate_markers)
    print "Task 1.1 completed successfully \n"
    
    
#Task1.2: duplicates.txt -> duplicate_markers_rsids.txt

@transform(write_out_duplicate_markers,                                    # Input             = qcplink
           suffix('.WriteOutDuplicates.Success'),                          # Input suffix      = WriteOutDuplicates.Success
           '.ExtractDuplicateMarkerRsids.Success')                         # Output suffix     = .ExtractDuplicateMarkerRsids.Success
           
def extract_duplicate_marker_rsids(inputs, outputs):
    """
    Extract rsids of duplicate markers from duplicates.txt.
    """
    
    flagFile = outputs
    
    print "Extract rsids of duplicate markers from duplicates.txt"
    runStageCheck('extract_duplicate_marker_rsids', flagFile)
    print "Task 1.2 completed successfully \n"


#Task1.3: duplicate_markers_rsids.txt -> qcplink.{fam, bim, bed} aka qcplink_bfiles
    
@transform(extract_duplicate_marker_rsids,                                 # Input             = qcplink
           suffix('.ExtractDuplicateMarkerRsids.Success'),                 # Input suffix      = .ExtractDuplicateMarkerRsids.Success
           add_inputs("duplicate_markers_rsids.txt"),                      # additional Inputs = duplicate_markers_rsids.txt
           '.RemoveDuplicateMarkers.Success')                              # Output suffix     = .RemoveDuplicateMarkers.Success
           
def remove_duplicate_markers(inputs, outputs):

    """
    Remove duplicates markers and generate qcplink.{fam, bim, bed}.
    """
    
    duplicate_marker_rsids = inputs[1]
    
    flagFile = outputs
    print "Removing duplicates markers and generate qcplink.{fam, bim, bed}."
    runStageCheck('remove_duplicate_markers', flagFile, orig_plink_bfiles, duplicate_marker_rsids)
    print "Task 1.3 completed successfully \n"




""" Tasks 1.4-2: Identify individuals with discordant sex information """

#Task1.4: qcplink_bfiles -> sexstat.sexcheck

@transform(remove_duplicate_markers,                                # Input             = qcplink
           suffix('.RemoveDuplicateMarkers.Success'),               # Input suffix      = .RemoveDuplicateMarkers.Success
           '.Sexcheck.Success')                                     # Output suffix     = .Sexcheck.Success 

def identify_indiv_discordant_sexinfo(inputs, outputs):
    """
    Identify individuals with discordant sex information.
    """
    flagFile = outputs
    print "Identifying individuals with discordant sex information."
    print "results will be written to sexstat.sexcheck"
    runStageCheck('identify_indiv_discordant_sexinfo', flagFile, qcplink_bfiles)
    print "Task 1.4 completed successfully \n"
    
    
#Task2: sexstat.sexcheck -> fail_sex_check_qcplink.txt
@transform(identify_indiv_discordant_sexinfo,                       # Input             = qcplink
           suffix('.Sexcheck.Success'),                             # Input suffix      = .Sexcheck.Success
           add_inputs('sexstat.sexcheck'),                          # additional Inputs = sexstat.sexcheck
           '.FailSexcheck.Success')                                 # Output suffix     = .FailSexcheck.Success
           
def find_fail_sexcheck(inputs, outputs):
    """
    Select individuals with Status="PROBLEM" in the  file sexstat.sexcheck.
    """
    sexstat = inputs[1]
    flagFile = outputs
    print "Selecting individuals with Status=PROBLEM in the file sexstat.sexcheck"
    print "results will be written to fail_sex_check_qcplink.txt"
    runStageCheck('find_fail_sexcheck', flagFile, sexstat)
    print "Task 2 completed successfully \n"
    



""" Tasks 3-5: Identification of individuals with elevated missing data rates or outlying heterozygosity rate """
    
#Task3: qcplink_bfiles -> qcplink_miss.imiss and qcplink_miss.lmiss

@transform(find_fail_sexcheck,                                      # Input             = qcplink
           suffix('.FailSexcheck.Success'),                         # Input suffix      = .FailSexcheck.Success
           '.SampleMissingness.Success')                            # Output suffix     = .SampleMissingness.Success
           
def calc_sample_missingness(inputs, outputs):
    """
    Calculate missingness scores for each individual.
    """
    flagFile = outputs
    print "Calculating missingness scores for each individual"
    print "results will be written to qcplink_miss.imiss and qcplink_miss.lmiss"
    runStageCheck('calc_sample_missingness', flagFile, qcplink_bfiles)
    print "Task 3 completed successfully \n"
    
    
#Task4: qcplink_bfiles -> qcplink_het.hh and qcplink_het.het

@transform(calc_sample_missingness,                                 # Input             = qcplink
           suffix('.SampleMissingness.Success'),                    # Input suffix      = .SampleMissingness.Success
           '.SampleHeterozygosity.Success')                         # Output suffix     = .SampleHeterozygosity.Success
           
def calc_sample_heterozygosity(inputs, outputs):
    """
    Calculate heterozygosity scores for each individual.
    """
    flagFile = outputs
    print "Calculating heterozygosity scores for each individual"
    print "results will be written to qcplink_het.hh and qcplink_het.het"
    runStageCheck('calc_sample_heterozygosity', flagFile, qcplink_bfiles)
    print "Task 4 completed successfully \n"
    
    
#Task5: qcplink_miss.imiss and qcplink_het.het -> pairs.imiss-vs-het.pdf (generates a plot)

@transform(calc_sample_heterozygosity,                              # Input             = qcplink
           suffix('.SampleHeterozygosity.Success'),                 # Input suffix      = .SampleHeterozygosity.Success
           '.MissHetplot.Success')                                  # Output suffix     = .MissHetplot.Success
           
def generate_miss_het_plot(inputs, outputs):
    """
    Plot the distribution of missingness and heterozygosity scores using the script miss_het_plot_qcplink.R.
    """
    flagFile = outputs
    print "Plotting the distribution of missingness and heterozygosity scores"
    print "results will be written to qcplink_plots dir"
    runStageCheck('generate_miss_het_plot', flagFile)
    print "Task 5 completed successfully \n"











 
    
 



