#!/bin/env python

""" pipeline_qcplink_tasks15-20of20_stages_config.py

    -Configuration file to set options specific to each stage/task in pipeline_qcplink_tasks15-20of20.py
=============================================================================
"""
import os

import PlinkUserInput as I

import WitsgwasSoftware as SW

python = SW.python
plink = SW.plink
plink1 = SW.plink1
perl = SW.perl
R = SW.R



# stageDefaults contains the default options which are applied to each stage (command).
# This section is required for every Rubra pipeline.
# These can be overridden by options defined for individual stages, below.
# Stage options which Rubra will recognise are: 
#  - distributed: a boolean determining whether the task should be submitted to a cluster
#      job scheduling system (True) or run on the system local to Rubra (False). 
#  - walltime: for a distributed PBS job, gives the walltime requested from the job
#      queue system; the maximum allowed runtime. For local jobs has no effect.
#  - memInGB: for a distributed PBS job, gives the memory in Gigabytes requested from the 
#      job queue system. For local jobs has no effect.
#  - queue: for a distributed PBS job, this is the name of the queue to submit the
#      job to. For local jobs has no effect. This is currently a mandatory field for
#      distributed jobs, but can be set to None.
#  - modules: the modules to be loaded before running the task. This is intended for  
#      systems with environment modules installed. Rubra will call module load on each 
#      required module before running the task. Note that defining modules for individual 
#      stages will override (not add to) any modules listed here. This currently only
#      works for distributed jobs.



stageDefaults = {
    'distributed': True,
    'queue': 'WitsLong',
    'walltime': "6:00:00",
    'memInGB': 16,
    'name': None,
    'modules': [
#         python,
#         plink,
#         perl,
#         R,
          'gwaspipe',
    ]
}




# stages should hold the details of each stage which can be called by runStageCheck.
# This section is required for every Rubra pipeline.
# Calling a stage in this way carries out checkpointing and, if desired, batch job
# submission. 
# Each stage must contain a 'command' definition. See stageDefaults above for other 
# allowable options.

if I.sexinfo_available:
	stages = {
		'find_SNPs_extreme_diff_miss': {
			"command": perl + " ../../select_diffmiss_qcplink.pl %cut_diff_miss"
		},
		'find_SNPs_extreme_HWE_deviations': {
			'command': plink + " --bfile %sample_qced_plink_bfiles --hardy --out clean_inds_qcplink_hwe"
		},
		'find_unaffected_for_HWE_plot': {
			'command': "head -1 clean_inds_qcplink_hwe.hwe > clean_inds_qcplink_hweu.hwe | grep 'UNAFF' clean_inds_qcplink_hwe.hwe >> clean_inds_qcplink_hweu.hwe"
		},
		'generate_hwe_plot': {
			'command': "Rscript ../../hwe_plot_qcplink.R"
		},
		'remove_SNPs_failing_Qc': {
			'command': plink + " --bfile %sample_qced_plink_bfiles --maf %cut_maf --geno %cut_geno --exclude %fail_diffmiss --hwe %cut_hwe --make-bed --out clean_qcplink"
		},
		'find_xchr_SNPs': {
			'command': plink + " --bfile %SNP_qced_plink_bfiles --chr 23 --make-bed --out xsnps"
		},
		'remove_xchr_SNPs': {
			'command': plink + " --bfile %SNP_qced_plink_bfiles --exclude %x_SNPs_bim --make-bed --out qced_plink"
		},
                'prune_qced_plink_for_pca': {
                        'command': plink + " --bfile %fully_qced_plink_bfiles --indep-pairwise 50 5 0.2 --out qced_plink_pruned"
                },
                'extract_prunein_qcedplink_for_pca': {
                        'command': plink + " --bfile %fully_qced_plink_bfiles --extract %prune_in_snps --make-bed --out qced_plink_pruned"
                },
                'do_pca_on_pruned_qcedplink': {
                        'command': "bash ../../runpca.sh qced_plink_pruned"
                },
                'do_twstats_on_pca_results': {
                        'command': "twstats -t ../../twtable -i %qcedplink_pca_eval -o qcedplink_pca.tw | echo 'twstats run on pca results of qced plink bfiles'"
                },
            }
	
else:
	stages = {
		'find_SNPs_extreme_diff_miss': {
			"command": perl + " ../../select_diffmiss_qcplink.pl %cut_diff_miss"
		},
		'find_SNPs_extreme_HWE_deviations': {
			'command': plink1 + " --noweb --bfile %sample_qced_plink_bfiles --allow-no-sex --hardy --out clean_inds_qcplink_hwe"
		},
		'find_unaffected_for_HWE_plot': {
			'command': "head -1 clean_inds_qcplink_hwe.hwe > clean_inds_qcplink_hweu.hwe | grep 'UNAFF' clean_inds_qcplink_hwe.hwe >> clean_inds_qcplink_hweu.hwe"
		},
		'generate_hwe_plot': {
			'command': "Rscript ../../hwe_plot_qcplink.R"
		},
		'remove_SNPs_failing_Qc': {
			'command': plink1 + " --noweb --bfile %sample_qced_plink_bfiles --allow-no-sex --maf %cut_maf --geno %cut_geno --exclude %fail_diffmiss --hwe %cut_hwe --make-bed --out clean_qcplink"
		},
		'find_xchr_SNPs': {
			'command': "touch xsnps.bim | echo no sex info available hence no x chromosome to find in %SNP_qced_plink_bfiles"
		},
		'remove_xchr_SNPs': {
			'command': plink + " --bfile %SNP_qced_plink_bfiles --exclude %x_SNPs_bim --make-bed --out qced_plink"
		},
                'prune_qced_plink_for_pca': {
                        'command': plink + " --bfile %fully_qced_plink_bfiles --allow-no-sex --indep-pairwise 50 5 0.2 --out qced_plink_pruned"
                },
                'extract_prunein_qcedplink_for_pca': {
                        'command': plink + " --bfile %fully_qced_plink_bfiles --allow-no-sex --extract %prune_in_snps --make-bed --out qced_plink_pruned"
                },
                'do_pca_on_pruned_qcedplink': {
                        'command': "bash ../../runpca.sh qced_plink_pruned"
                },
                'do_twstats_on_pca_results': {
                        'command': "twstats -t ../../twtable -i %qcedplink_pca_eval -o qcedplink_pca.tw | echo 'twstats run on pca results of qced plink bfiles'"
                },
            }
