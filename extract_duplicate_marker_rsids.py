#!/bin/env python

def extract_duplicate_marker_rsids(file, newfile):

    # read lines in duplicates.txt into a list called duplicates
    with open(newfile, 'w') as outfile, open(file, 'r') as infile:
        

        duplicates = infile.readlines() 

        for item in duplicates:
        # ignoring commment lines
            if (item.startswith('#') or item.startswith('\n')):
                pass
            else:
                # splitting on empty space, extract fifth field (contains rsids)
                item = item.split(" ")
                print item
                duplicate_snp = item[5].strip()
                print duplicate_snp
                outfile.write(duplicate_snp + '\n')
        
        
extract_duplicate_marker_rsids("duplicates.txt","duplicate_markers_rsids.txt")
