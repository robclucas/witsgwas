#!/bin/env python

""" pipeline_qcaffymetrix_config.py 

    -Configuration file to set input files, directories and parameters 
    specific to pipeline_qcaffymetrix.py
=============================================================================
"""

import os
import WitsgwasScripts as SC 
import AffymetrixUserInput as I


# This section is used by the pipeline_qcaffymetrix.py to specify input data and 
# working directories.

# Required inputs:
# 1. path to the raw .CEL files directory 
# 2. project name (data type: string)
# 3. project author (data type: string) needed for record keeping

'''
note: project name will be used by the pipeline to generate a 
time stamped output directory '''


working_files = {
    'celfiles_dir': I.celfiles_dir,
    'projectname': I.projectname,
    'projectauthor': I.author,
    'plink_cases': I.plink_phenotypes_cases
}


# This section is used by the pipeline_qcaffymetrix.py to specify affymetrix annotation
# files and qc libraries.
# (http://www.affymetrix.com/site/include/byproduct.affx?product=genomewidesnp_6)

# affy_geno_qc required files:
# - affy_cdf: Contains the description of the probe sets on the chip
# - affy_r2_qcc: File defining the QC probe sets on the chip
# - affy_r2_qca: File defining the QC analysis methods to run

# affy_geno_probeset required files:
# - affy_cdf: Contains the description of the probe sets on the chip
# - affy_chrXprobes: File defining the probes for chromosome X 
# - affy_chrYprobes: File defining the probes for chromosome Y 
# note: the X and Y probes are for copy number probe chrX/Y ratio gender calling


#conversion from birdseed to plink format required files:
# - affy_annotation: Affymetrix GenomeWideSNP6 annotation file (currently using: release 34)


affy_library = {
    'affy_qc_lib': I.affy_qc_lib,
    'affy_annotation': I.affy_annotation    
}



# This section is used by the pipeline_qcaffymetrix.py to specify configuration options 
# for itself (pipeline_qcaffymetrix.py) as well as Rubra. 

# Rubra variables:
#  - logDir: the directory where batch queue scripts, stdout and sterr dumps are stored.
#  - logFile: the file used to log all jobs that are run.
#  - style: the default style, one of 'flowchart', 'print', 'run', 'touchfiles'. Can be 
#      overridden by specifying --style on the command line.
#  - procs: the number of python processes to run simultaneously. This determines the
#      maximum parallelism of the pipeline. For distributed jobs it also constrains the
#      maximum total jobs submitted to the queue at any one time.
#  - verbosity: one of 0 (quiet), 1 (normal), 2 (chatty). Can be overridden by specifying
#      --verbose on the command line.
#  - end: the desired tasks to be run. Rubra will also run all tasks which are dependencies 
#      of these tasks. Can be overridden by specifying --end on the command line.
#  - force: tasks which will be forced to run, regardless of timestamps. Can be overridden
#      by supplying --force on the command line.
#  - rebuild: one of 'fromstart','fromend'. Whether to calculate which dependencies will
#      be rerun by working back from an end task to the latest up-to-date task, or forward
#      from the earliest out-of-date task. 'fromstart' is the most conservative and 
#      commonly used as it brings all intermediate tasks up to date.


# pipeline_qcaffymetrix variables:
# nothing at this stage, but could be used to add features for batch qc in future

pipeline = {
    'logDir': os.path.join(SC.CURRENT_PROJECT_DIR, "log_qcaffymetrix"),
    'logFile': 'pipeline_qcaffymetrix.log',
    'style': 'print',
    'procs': 30,
    'verbose': 2,
    'end': ['update_plink_phenotypes' 
            ],
    'force': [],
    'rebuild' : "fromstart",

    'restrict_samples': False,
    'allowed_samples': []
    
}
