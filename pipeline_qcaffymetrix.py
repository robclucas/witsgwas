#!/bin/env python

""" pipeline_qcaffymetrix.py 

    -SNP chip genotype calling pipeline for GWAS using Affymetrix Power Tools.
=============================================================================

Authors: Lerato Magosi, Scott Hazelhurst.

This program implements a workflow for human GWAS analysis using data from the 
Affymetrix GenomeWideSNP_6 chip/microarray; starting from the raw .CEL file stage.

It uses the GenomeWideSNP_6.birdseed-v2 genotype calling algorithm in
Affymetrix Power Tools (APT) version: 1.15.2

Following genotype calling, this program converts the birdseed calls to plink format
(.map and .ped) files.

It employs Rubra for sending jobs to a linux cluster via PBS Torque (version 2.5). 
Rubra is a pipeline system for bioinformatics workflows that is built on top
of the Ruffus (http://www.ruffus.org.uk/) Python library. Rubra adds support for running 
pipeline stages on a distributed computer cluster (https://github.com/bjpop/rubra) and
also supports parallel evaluation of independent pipeline stages.

The pipeline is configured by an options file in a python file,
including the actual commands which are run at each stage.

"""

# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)


# pandas imports 
# import numpy as np
# import scipy as sp
# from pandas import *
# from rpy2.robjects.packages import importr
# import rpy2.robjects as ro
# import pandas.rpy.common as com
from pyfiglet import Figlet



# user defined module imports
# import pipeline_qcaffymetrix_config as conf

import Filemanager as FM

import WitsgwasSoftware as SW

import WitsgwasScripts as SC  




# Shorthand access to options defined in pipeline_qcaffymetrix_config.py
#==========================================

affy_library = pipeline_options.affy_library
working_files = pipeline_options.working_files
logDir = pipeline_options.pipeline['logDir']


# Data setup process and input organisation
#==========================================

f = Figlet(font='standard')
print f.renderText('witsGWAS')
print "(C) 2015 Lerato E. Magosi, Scott Hazelhurst"
print "http://magosil86.github.io/witsGWAS/"    
print "witsGWAS v0.1.0 is licensed under the MIT license. See LICENSE.txt"
print "----------------------------------------------------------------"


# create a directory for the current project
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = (os.path.join(SC.witsGWAS_PROJECTS_DIR, working_files['projectname']) + 
	'-qcaffy-' + working_files['projectauthor'] + '-' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '/')

#SC.CURRENT_PROJECT_DIR = ('/home/lerato/witsGWAS/projects/h3data-2015-08-10_13-11-32/') 
print "Current project directory %s" % SC.CURRENT_PROJECT_DIR

FM.create_dir(SC.CURRENT_PROJECT_DIR)


# create a file listing the paths of all the .CEL in the celfiles dir.
#conf.working_files['celiles_dir'] = "/Users/lmagosi/Desktop"

global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"

celfiles = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "celfiles.txt")
print "path to celfiles.txt is %s" % celfiles

# cd into the celfiles dir.
os.chdir(SC.CURRENT_PROJECT_DIR)

# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir

FM.create_emptyfile(celfiles)


# cd into the celfiles dir.
os.chdir(working_files['celfiles_dir']) 

subprocess.call('ls -1d $PWD/* > %s' % celfiles, shell=True)


# add the word: cel_files to the first line of the celfiles.txt 
# note: identifier needed by Affymetrix Power Tools

# cd back to current project dir.
os.chdir(os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR))
 

# prepend cel_files to celfiles.txt
line = "cel_files"
FM.line_prepender(celfiles, line)


# create a dir. for converting birdseed calls to plink format
birdseed_to_plink = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "birdseed_to_plink") + '/') 
FM.create_dir(birdseed_to_plink)

# create a dir for failed affygenoqc .CEL files
failed_genoqc_cell_files = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "failed_genoqc_cell_files") + '/') 
FM.create_dir(failed_genoqc_cell_files)

# create a dir for failed affygenoprobeset .CEL files
failed_probeset_geno_cell_files = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "failed_probeset_geno_cell_files") + '/') 
FM.create_dir(failed_probeset_geno_cell_files)


# Paths to intermediate result files
#==========================================

# affygenoqc
path2genoqcresults = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "celfiles.genoqcresults")
path2genoqcfailed = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "celfiles.genoqcfailed")
path2genoqcfiltered = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "celfiles.genoqcfiltered")

# affygenoprobeset
path2probeset_geno_results = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "probeset_geno_results") + '/')
path2birdseedcalls = os.path.join(path2probeset_geno_results, "birdseed-v2.calls.txt")
path2birdseedconfidence = os.path.join(path2probeset_geno_results, "birdseed-v2.confidences.txt")
path2birdseedreport = os.path.join(path2probeset_geno_results, "birdseed-v2.report.txt")

# affygenoprobeset-2nd-pass
path2probeset_geno_results_2nd_pass = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "probeset_geno_results_2nd_pass") + '/')
path2birdseedcalls_2nd_pass = os.path.join(path2probeset_geno_results_2nd_pass, "birdseed-v2.calls.txt")
path2birdseedconfidence_2nd_pass = os.path.join(path2probeset_geno_results_2nd_pass, "birdseed-v2.confidences.txt")
path2birdseedreport_2nd_pass = os.path.join(path2probeset_geno_results_2nd_pass, "birdseed-v2.report.txt")



# Optional tasks
#==========================================

#specify cases and controls
if len(working_files['plink_cases']) == 0:
    #path to case list file Not provided
    generate_pheno = False
else:
    #path to case list file provided
    generate_pheno = True



# Print project information
#==========================================

print "Starting project %s" % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print

    

# Pipeline declarations
#==========================================


# Phase 1 - Measure data intensity quality.

# Important1: If your data was genotyped in batches then you will need to 
# run apt-geno-qc for each batch.
# Important2: If data is indeed in batches, it may be advisable to 
# use parallelization when running apt-geno-qc.


#Task1: celfiles.txt -> celfiles.genoqcresults

annot= os.path.join(affy_library['affy_annotation'],"GenomeWideSNP_6.na34.annot.csv")

cdf= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.cdf")

qcc= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.r2.qcc")

qca= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.r2.qca")

@transform(celfiles,                                              # Input             = celffiles
           suffix('.txt'),                                        # Input suffix      = .txt
           add_inputs(cdf, qcc, qca),                             # additional Inputs = cdf, qcc and qca
           ['.genoqcresults', '.genoqcresults.Success'])          # Output suffices   = .genoqcresults and .genoqcresults.Success 

def do_affygenoqc(inputs, outputs):
    """
    Run affy-geno-qc on each .CEL file listed in celfiles.txt.
    """
    file, cdf, qcc, qca  = inputs
    output, flagFile = outputs
    print "Running affy-geno-qc on all .CEL files in celfiles.txt"
    runStageCheck('do_affygenoqc', flagFile, cdf, qcc, qca, output, file)
    print "Task 1 completed successfully \n"


#Task2: celfiles.genoqcresults -> celfiles.genoqcresultsSortedQCR
@transform(do_affygenoqc,                                                       # Input           = celffiles
           suffix('.genoqcresults'),                                            # Input suffix    = .genoqcresults
           ['.genoqcresultsSortedQCR', '.genoqcresultsSortedQCR.Success'])      # Output suffices = .genoqcresultsSortedQCR and .genoqcresultsSortedQCR.Success 

def sort_affygenoqc_byQCR(inputs, outputs):
    """
    Sort celfiles.genoqcresults by QCR to check if have samples below 86%.
    QCR: quality control rate
    """
    file = inputs[0]                                                            # leave flagFile behind and only use celfiles.genoqcresults 
    output, flagFile = outputs
    print "Writing out celfiles.genoqcresults sorted by quality contol rate (QCR); to check if have QCR < 86%."
    runStageCheck('sort_affygenoqc_byQCR', flagFile, file, output)
    print "Task 2 completed successfully \n"


#Task3: celfiles.genoqcresults -> celfiles.genoqcresultsSortedCQC
@follows(sort_affygenoqc_byQCR)
@transform(do_affygenoqc,                                                       # Input           = celffiles
           suffix('.genoqcresults'),                                            # Input suffix    = .genoqcresults
           ['.genoqcresultsSortedCQC', '.genoqcresultsSortedCQC.Success'])      # Output suffices = .genoqcresultsSortedCQC and .genoqcresultsSortedCQC.Success 

def sort_affygenoqc_byCQC(inputs, outputs):
    """
    Sort celfiles.genoqcresults by CQC to check if have samples below 0.4.
    CQC: contrast quality control
    """
    file = inputs[0]                                                            # leave flagFile behind and only use celfiles.genoqcresults 
    output, flagFile = outputs
    print "Writing out celfiles.genoqcresults sorted by contrast quality control (CQC); to check if have CQC < 0.4%."
    runStageCheck('sort_affygenoqc_byCQC', flagFile, file, output)
    print "Task 3 completed successfully \n"

#Task4: celfiles.genoqcresults -> celfiles.genoqcfailed
@follows(sort_affygenoqc_byCQC)
@transform(do_affygenoqc,                                   # Input           = celffiles
           suffix('.genoqcresults'),                        # Input suffix    = .genoqcresults
           ['.genoqcfailed', '.genoqcfailed.Success'])      # Output suffices = .genoqcfailed and .genoqcfailed.Success 

def find_failed_affygenoqc(inputs, outputs):
    """
    Write out samples that did not meet the QCR and CQC thresholds.
    QCR: quality control rate
    CQC: contrast quality control
    """
    file = inputs[0]                                        # leave flagFile behind and only use celfiles.genoqcresults 
    output, flagFile = outputs
    print "Writing out samples that did not meet the QCR (0.86) and CQC (0.4) thresholds to celfiles.genoqcfailed."
    runStageCheck('find_failed_affygenoqc', flagFile, file, output)
    print "Task 4 completed successfully \n"


# Task5: celfiles.genoqcresults -> celfiles.genoqcfiltered
@follows(find_failed_affygenoqc)
@transform(do_affygenoqc,                                       # Input           = celffiles
           suffix('.genoqcresults'),                            # Input suffix    = .genoqcresults
           ['.genoqcfiltered', '.genoqcfiltered.Success'])      # Output suffices = .genoqcfiltered and .genoqcfiltered.Success 
def filter_passed_affygenoqc(inputs, outputs):
    """
    Generate a new filtered file excluding samples that did not meet the QCR and CQC thresholds.
    """
    file = inputs[0]                                            # leave flagFile behind and only use celfiles.genoqcresults 
    output, flagFile = outputs
    print "Writing out samples that passed affy-geno-qc to celfiles.genoqcfiltered."    
    runStageCheck('filter_passed_affygenoqc', flagFile, file, output)
    print "Task 5 completed successfully \n"


# Task6: celfiles.genoqcfiltered -> celfiles.genoqcfiltered
@transform(filter_passed_affygenoqc,                                       # Input           = celffiles
           suffix('.genoqcfiltered'),                                      # Input suffix    = .genoqcfiltered
           ['.genoqcfilteredMeanCQC', '.genoqcfilteredMeanCQC.Success'])     # Output suffices   = .genoqcfilteredMeanCQC and .genoqcfilteredMeanCQC.Success 
def find_mean_cqc(inputs, outputs):
    """
    Calculate the mean of the values in the CQC field of the celfiles.genoqcfiltered file.
    """
    file = inputs[0]                                            # leave flagFile behind and only use .genoqcfiltered 
    output, flagFile = outputs
    print ("Calculating the mean of the values in the CQC field of the " 
    "celfiles.genoqcfiltered file \n. Affymetrix recommended threshold is mean CQC > 1.7. \n"
    "A mean CQC < 1.7 may signal poorly resolved clusters leading to a low genotyping call rate.")
    runStageCheck('find_mean_cqc', flagFile, file, output)
    print "Task 6 completed successfully \n"

# Task7: celfiles.genoqcfiltered -> celfiles.namesgenoqcfiltered
@follows(find_mean_cqc)
@transform(filter_passed_affygenoqc,                                   # Input           = celffiles
           suffix('.genoqcfiltered'),                                  # Input suffix    = .genoqcfiltered
           ['.namesgenoqcfiltered', '.namesgenoqcfiltered.Success'])   # Output suffices = .namesgenoqcfiltered and .namesgenoqcfiltered.Success 
def extract_namesof_genoqcfiltered(inputs, outputs):
    """
    Extract the names of the files that passed affygenoqc from celfiles.genoqcfiltered
    """
    file = inputs[0]                                                   # leave flagFile behind and only use celfiles.genoqcfiltered 
    output, flagFile = outputs
    print "Extracting the names of files that passed affygenoqc from celfiles.genoqcfiltered. \n"    
    runStageCheck('extract_namesof_genoqcfiltered', flagFile, file, output)
    print "Task 7 completed successfully \n"


# Task8: celfiles.namesgenoqcfiltered -> celfiles.path2genoqcfiltered
@transform(extract_namesof_genoqcfiltered,                                   # Input           = celffiles
           suffix('.namesgenoqcfiltered'),                                   # Input suffix    = .namesgenoqcfiltered
           add_inputs(celfiles),
           ['.path2genoqcfiltered', '.path2genoqcfiltered.Success'])         # Output suffices = .path2genoqcfiltered and .path2genoqcfiltered.Success 
def extract_paths2genoqcfiltered(inputs, outputs):
    """
    Use celfiles.namesgenoqcfiltered to extract the paths of files that passed
    affygenoqc from celfiles.txt
    """
    file = inputs[0][0]                                                        # leave flagFile behind and only use celfiles.namesgenoqcfiltered 
    celfiles = inputs[1]
    output, flagFile = outputs
    print ("Using celfiles.namesgenoqcfiltered to extract the paths of files that passed "
    "affygenoqc from celfiles.txt")    
    runStageCheck('extract_paths2genoqcfiltered', flagFile, file, celfiles, output)
    print "Task 8 completed successfully \n"




# Phase 2 - use the celfiles that passed affygenoqc to run apt-geno-probeset


# Task9: .CEL -> probeset_geno_results


cdf= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.cdf")

ss= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.specialSNPs")

xpb= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.chrXprobes")

ypb= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.chrYprobes")

mdl= os.path.join(affy_library['affy_qc_lib'],"GenomeWideSNP_6.birdseed-v2.models")

@transform(extract_paths2genoqcfiltered,                          # Input             = celfiles
           suffix('.path2genoqcfiltered'),                        # Input suffix      = path2genoqcfiltered
           add_inputs(cdf, ss, xpb, ypb, mdl),                    # additional Inputs = cdf, ss, xpb, ypb and mdl
           '.genoprobesetresults.Success')                        # Output suffix     = .genoprobesetresults.Success 

def do_affygenoprobeset(inputs, outputs):
    """
    Run affy-geno-probeset on each .CEL file specified in celfiles.path2genoqcfiltered
    """
    
    filtered_celfiles_paths = inputs[0][0]
    cdf, ss, xpb, ypb, mdl  = inputs[1:]                          # leave flagFile behind, only use cdf, ss, xpb, ypb and mdl
    flagFile = outputs
    output_dir = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "probeset_geno_results") 
    
    print "Running affy-geno-probeset on all .CEL files in the celfiles.path2genoqcfiltered file."
    print "The results will be placed in the probeset_geno_results dir."
    runStageCheck('do_affygenoprobeset', flagFile, output_dir, cdf, xpb, ypb, ss, mdl, filtered_celfiles_paths)
    print "Task 9 completed successfully \n"
    
    
# Task10: probeset_geno_results -> celfiles.birdseedreportSortedCR

@transform(do_affygenoprobeset,                                                  # Input             = celfiles
           suffix('.genoprobesetresults.Success'),                               # Input suffix      = genoprobesetresults.Success
           add_inputs(path2birdseedreport),                                      # additional Inputs = path2birdseedreport
           ['.birdseedreportSortedCR', '.birdseedreportSortedCR.Success'])       # Output suffices   = .birdseedreportSortedCR and .birdseedreportSortedCR.Success 

def sort_birdseedreport_byCR(inputs, outputs):
    """
    Sort birdseed-v2.report.txt by CR to check if have samples below 97%.
    CR: call rate
    """    
       
    file = inputs[1]                                                             # leave flagFile behind and only use birdseed_report                                                             
    output, flagFile = outputs
    print "Sorting birdseed-v2.report.txt by CR to check if have CR < 97%."
    runStageCheck('sort_birdseedreport_byCR', flagFile, file, output)
    print "Task 10 completed successfully \n"
    
    
# Task11: celfiles.birdseedreportSortedCR -> celfiles.birdseedreportfailed
@transform(sort_birdseedreport_byCR,                                            # Input             = celfiles
           suffix('.birdseedreportSortedCR'),                                   # Input suffix      = .birdseedreportSortedCR
           ['.birdseedreportfailed', '.birdseedreportfailed.Success'])          # Output suffices   = .birdseedreportfailed and .birdseedreportfailed.Success 

def find_failed_birdseedreport(inputs, outputs):
    """
    Write out samples that did not meet the CR threshold.
    CR: call rate
    """
        
    file = inputs[0]                                                            # leave flagFile behind and only use celfiles.birdseedreportSortedCR                                                              
    output, flagFile = outputs                                                   
    print "Writing out samples that did not meet the CR threshold i.e (CR < 97) to celfiles.birdseedreportfailed."
    runStageCheck('find_failed_birdseedreport', flagFile, file, output)
    print "Task 11 completed successfully \n"


# Task12: celfiles.birdseedreportSortedCR -> celfiles.birdseedreportfiltered
@follows(find_failed_birdseedreport)
@transform(sort_birdseedreport_byCR,                                            # Input             = celfiles
           suffix('.birdseedreportSortedCR'),                                   # Input suffix      = .birdseedreportSortedCR
           ['.birdseedreportfiltered', '.birdseedreportfiltered.Success'])      # Output suffices   = .birdseedreportfiltered and .birdseedreportfiltered.Success 

def filter_passed_affygenoprobeset(inputs, outputs):
    """
    Generate a new filtered file excluding samples that did not meet the CR threshold.
    CR: call rate
    """
        
    file = inputs[0]                                                            # leave flagFile behind and only use celfiles.birdseedreportSortedCR                                                         
    output, flagFile = outputs
    print "Writing out samples that passed affy-geno-probeset (CR >= 0.97) to celfiles.birdseedreportfiltered."
    runStageCheck('filter_passed_affygenoprobeset', flagFile, file, output)
    print "Task 12 completed successfully \n"
    
    
# Task13: celfiles.birdseedreportfiltered -> celfiles.namesbirdseedreportfiltered
@transform(filter_passed_affygenoprobeset,                                             # Input           = celffiles
           suffix('.birdseedreportfiltered'),                                          # Input suffix    = .birdseedreportfiltered
           ['.namesbirdseedreportfiltered', '.namesbirdseedreportfiltered.Success'])   # Output suffices = .namesbirdseedreportfiltered and .namesbirdseedreportfiltered.Success 
def extract_namesof_genoprobesetfiltered(inputs, outputs):
    """
    Extract the names of the files that passed affygenoprobeset from celfiles.birdseedreportfiltered
    """
    file = inputs[0]                                                                   # leave flagFile behind and only use celfiles.birdseedreportfiltered
    output, flagFile = outputs
    print "Extracting the names of files that passed affygenoprobeset from celfiles.birdseedreportfiltered. \n"    
    runStageCheck('extract_namesof_genoprobesetfiltered', flagFile, file, output)
    print "Task 13 completed successfully \n"
    

# Task14: celfiles.namesbirdseedreportfiltered -> celfiles.path2birdseedreportfiltered
@transform(extract_namesof_genoprobesetfiltered,                                             # Input           = celffiles
           suffix('.namesbirdseedreportfiltered'),                                           # Input suffix    = .namesbirdseedreportfiltered
           add_inputs(celfiles),
           ['.path2birdseedreportfiltered', '.path2birdseedreportfiltered.Success'])         # Output suffices = .path2birdseedreportfiltered and .path2birdseedreportfiltered.Success 
def extract_paths2birdseedreportfiltered(inputs, outputs):
    """
    Use celfiles.namesbirdseedreportfiltered to extract the paths of files that passed
    affygenoprobeset from celfiles.txt
    """
    file = inputs[0][0]                                                                      # leave flagFile behind and only use celfiles.namesbirdseedreportfiltered 
    celfiles = inputs[1]
    output, flagFile = outputs
    print ("Using celfiles.namesbirdseedreportfiltered to extract the paths of files that passed "
    "affygenoprobeset from celfiles.txt")    
    runStageCheck('extract_paths2birdseedreportfiltered', flagFile, file, celfiles, output)
    print "Task 14 completed successfully \n"
    

# Task15: .path2birdseedreportfiltered -> probeset_geno_results_2nd_pass
@transform(extract_paths2birdseedreportfiltered,                                   # Input             = celfiles
           suffix('.path2birdseedreportfiltered'),                                 # Input suffix      = .birdseedreportfiltered
           add_inputs(cdf, ss, xpb, ypb, mdl),                                     # additional Inputs = cdf, ss, xpb, ypb and mdl
           '.genoprobesetresults2ndpass.Success')                                  # Output suffix     = .genoprobesetresults2ndpass.Success 

def do_affygenoprobeset_2nd_pass(inputs, outputs):
    """
    Run affy-geno-probeset on each .CEL file specified in celfiles.path2birdseedreportfiltered
    """
    
    filtered_celfiles_paths = inputs[0][0]
    cdf, ss, xpb, ypb, mdl  = inputs[1:]                                           # leave flagFile behind, only use cdf, ss, xpb, ypb and mdl
    flagFile = outputs
    output_dir = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "probeset_geno_results_2nd_pass") 
    
    print "Running affy-geno-probeset on all .CEL files in the celfiles.path2birdseedreportfiltered file."
    print "The results will be placed in the probeset_geno_results_2nd_pass dir."
    runStageCheck('do_affygenoprobeset_2nd_pass', flagFile, output_dir, cdf, xpb, ypb, ss, mdl, filtered_celfiles_paths)
    print "Task 15 completed successfully \n"
 
    
# Task16: probeset_geno_results_2nd_pass  -> birdseed2plink

@transform(do_affygenoprobeset_2nd_pass,                                                    # Input             = celfiles
           suffix('.genoprobesetresults2ndpass.Success'),                                   # Input suffix      = .genoprobesetresults2ndpass.Success
           add_inputs(path2birdseedreport_2nd_pass, path2birdseedcalls_2nd_pass, annot),    # additional Inputs = path2birdseedreport_2nd_pass, path2birdseedcalls_2nd_pass and annot
           '.birdseed2plink.Success')                                                       # Output suffix     = .birdseed2plink.Success 

def birdseed2plink(inputs, outputs):
    """
    Convert birdseed calls to plink format
    """
    
    birdseedreport, birdseedcalls, annot = inputs[1:]                                        # leave flagFile behind, only use birdseedreport, birdseedcalls and annot
    flagFile = outputs
    output_dir = birdseed_to_plink
    
    print "Converting birdseed calls to plink format (.map and .ped)."
    print "The results will be placed in the birdseed_to_plink dir."
    runStageCheck('birdseed2plink', flagFile, birdseedcalls, birdseedreport, annot, output_dir)
    print "Task 16 completed successfully \n"

    
# Task17: mapfile.map -> celfiles.map 

mapfile = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "birdseed_to_plink", "mapfile.map")

@transform(birdseed2plink,                                                                  # Input             = celfiles
           suffix('.birdseed2plink.Success'),                                               # Input suffix      = .birdseed2plink.Success
           add_inputs(mapfile),                                                             # additional Inputs = mapfile
           ['.map', '.map.Success'])                                                        # Output suffix     = .map and .map.Success 

def copy_mapfile(inputs, outputs):
    """
    Plink uses files with common basenames. \n
    Copy mapfile.map to celfiles.map
    """
    mapfile = inputs[1]                                                                     # leave flagFile behind, only use mapfile                                                             
    output, flagFile = outputs
    print "Copying mapfile.map to celfiles.map"
    runStageCheck('copy_mapfile', flagFile, mapfile, output)
    print "Task 17 completed successfully \n"
 

# Task18: pedfile.ped -> celfiles.ped

pedfile = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "birdseed_to_plink", "pedfile.ped") 
   
@transform(copy_mapfile,                                                                  # Input             = celfiles
           suffix('.map'),                                                                # Input suffix      = .map
           add_inputs(pedfile),                                                           # additional Inputs = pedfile
           ['.ped', '.ped.Success'])                                                      # Output suffix     = .ped and .ped.Success 

def copy_pedfile(inputs, outputs):
    """
    Plink uses files with common basenames. \n
    Copy pedfile.ped to celfiles.ped.
    """
    pedfile = inputs[1]                                                                   # leave flagFile behind, only use pedfile                                                          
    output, flagFile = outputs
    print "Copying pedfile.ped to celfiles.ped"
    runStageCheck('copy_pedfile', flagFile, pedfile, output)
    print "Task 18 completed successfully \n"
    
    
# Task19: celfiles.map -> celfiles_pre_add_rsid.map

celfiles_map = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "celfiles.map") 

@transform(copy_pedfile,                                                                  # Input             = celfiles
           suffix('.ped'),                                                                # Input suffix      = .ped
           add_inputs(celfiles_map),                                                      # additional Inputs = celfiles.map
           '.preaddrsid.Success')                                                         # Output suffices   = .preaddrsid.Success
           
def rename_celfiles_map_as_celfiles_preaddrsidmap(inputs, outputs):
    """
    Rename celfiles.map as celfiles_pre_add_rsid.map.
    """
    celfiles_map = inputs[1]                                                                
    flagFile = outputs
    output = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "celfiles_pre_add_rsid.map")
    print "Renaming celfiles.map as celfiles_pre_add_rsid.map"
    runStageCheck('rename_celfiles_map_as_celfiles_preaddrsidmap', flagFile, celfiles_map, output)
    print "Task 19 completed successfully \n"



# Task20: celfiles_pre_add_rsid.map -> celfiles.map (add dummy rsids)
@transform(rename_celfiles_map_as_celfiles_preaddrsidmap,                                 # Input             = celfiles
           suffix('.preaddrsid.Success'),                                                 # Input suffix      = .preaddrsid.Success
           '.addDummyRsid.Success')                                                       # Output suffices   = .addDummyRsid.Success
           
def add_dummy_rsids(inputs, outputs):
    """
    Replace missing rsids with dummy rsids.
    """
    flagFile = outputs
    print "Replacing missing rsids with dummy rsids."
    runStageCheck('add_dummy_rsids', flagFile)
    print "Task 20 completed successfully \n"


# Task21: celfiles.{map, ped} -> celfiles.{fam, bim, bed}
@transform(add_dummy_rsids,                                                               # Input             = celfiles
           suffix('.addDummyRsid.Success'),                                               # Input suffix      = .addDummyRsid.Success
           '.celfilesbinary.Success')                                                     # Output suffix     = .celfilesbinary.Success
           
def make_celfiles_binary(inputs, outputs):
    """
    Convert celfiles.{map, ped} to binary format (celfiles.{fam, bim, bed}).
    """           
    flagFile = outputs
    input_basename = "celfiles"
    output_basename = "celfiles"
    print "Converting celfiles.{map, ped} to binary format (celfiles.{fam, bim, bed})"
    runStageCheck('make_celfiles_binary', flagFile, input_basename, output_basename)
    print "Task 21 completed successfully \n"
    

# Task22: celfiles.{fam, bim, bed} -> celfiles.phenoCases
# only runs if generate_pheno == True
# @active_if(generate_pheno) # cannot use active_if, it is currently not defined in rubra

# Note: Currently using Rubra version: 0.1.5 which depends on ruffus version: 2.2, 
# active_if was only introduced in ruffus version 2.3

# So we'll use a work around. This hack! works here because we do not have downstream
# tasks following this decision, if we did ruffus would complain about missing dependencies.
if generate_pheno:
	@transform(make_celfiles_binary,                                                          # Input             = celfiles
			   suffix('.celfilesbinary.Success'),                                             # Input suffix      = .celfilesbinary.Success
			   add_inputs(working_files['plink_cases'], "celfiles.fam"),                      # additional Inputs = working_files['plink_cases'] and celfiles.fam
			   ['.phenoCases', '.phenoCases.Success'])                                        # Output suffices   = .phenoCases and .phenoCases.Success
		   
	def make_pheno_cases_file(inputs, outputs):
		"""
		Use case list (file containing names of celfiles designated as cases) supplied by user
		to extract familyID and individualID from celfiles.fam and save as celfiles.phenoCases 
		"""
		case_list, celfiles_fam = inputs[1:]
		output, flagFile = outputs
		print "Using user suppplied caselist to extract familyID and individualID from celfiles.fam"
		print "output will be saved as celfiles.phenoCases" 
		runStageCheck('make_pheno_cases_file', flagFile, case_list, celfiles_fam, output)
		print "Task 22 completed successfully \n"
		  
	
	# Task23: celfiles.phenoCases -> celfiles.{fam, bim, bed} (specify cases and controls)

	@transform(make_pheno_cases_file,                                                         # Input             = celfiles
			   suffix('.phenoCases'),                                                         # Input suffix      = .phenoCases
			   '.updateplinkpheno.Success')                                                   # Output suffices   = .updateplinkpheno.Success
					  
	def update_plink_phenotypes(inputs, outputs):
		"""
		Specify cases and controls in celfiles.{fam, bim, bed}.
		"""           
		phenocases = inputs[0]                                                                
		flagFile = outputs
		input_basename = "celfiles"
		output_basename = "celfiles"
		print "Specifying cases and controls in celfiles.{fam, bim, bed}"
		print ("Specifying cases and controls now distinguishes mapfile.map/celfiles.map and "
		"pedfile.ped/celfiles.ped from celfiles.{fam, bim, bed}")
		runStageCheck('update_plink_phenotypes', flagFile, input_basename, phenocases, output_basename)
		print "Task 23 completed successfully \n"
    



               



    
    


