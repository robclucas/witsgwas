#!/bin/env python

""" pipeline_assoc_testing_stages_config.py

    -Configuration file to set options specific to each stage/task in pipeline_assoc_testing.py
=============================================================================
"""
import os

import AdvancedAssocUserInput as I

import WitsgwasSoftware as SW

python = SW.python
plink = SW.plink
plink1 = SW.plink1
perl = SW.perl
R = SW.R



# stageDefaults contains the default options which are applied to each stage (command).
# This section is required for every Rubra pipeline.
# These can be overridden by options defined for individual stages, below.
# Stage options which Rubra will recognise are: 
#  - distributed: a boolean determining whether the task should be submitted to a cluster
#      job scheduling system (True) or run on the system local to Rubra (False). 
#  - walltime: for a distributed PBS job, gives the walltime requested from the job
#      queue system; the maximum allowed runtime. For local jobs has no effect.
#  - memInGB: for a distributed PBS job, gives the memory in Gigabytes requested from the 
#      job queue system. For local jobs has no effect.
#  - queue: for a distributed PBS job, this is the name of the queue to submit the
#      job to. For local jobs has no effect. This is currently a mandatory field for
#      distributed jobs, but can be set to None.
#  - modules: the modules to be loaded before running the task. This is intended for  
#      systems with environment modules installed. Rubra will call module load on each 
#      required module before running the task. Note that defining modules for individual 
#      stages will override (not add to) any modules listed here. This currently only
#      works for distributed jobs.



stageDefaults = {
    'distributed': True,
    'queue': 'WitsLong',
    'walltime': "6:00:00",
    'memInGB': 16,
    'name': None,
    'modules': [
#         python,
#         plink,
#         perl,
#         R,
          'gwaspipe',
    ]
}



# stages should hold the details of each stage which can be called by runStageCheck.
# This section is required for every Rubra pipeline.
# Calling a stage in this way carries out checkpointing and, if desired, batch job
# submission. 
# Each stage must contain a 'command' definition. See stageDefaults above for other 
# allowable options.

if I.sexinfo_available:
    stages = {
		'do_permutation_testing': {
			'command': plink + " --bfile %qced_plink_bfiles --model mperm=10000 --out qced_plink_permtest"
		},
		'sort_permtest_results': {
			'command': "sort -k4n -r %permtest_results > qced_plink_permtest_sorted.model.best.mperm"
		},
		'convert_evec2cov': {
			'command': "tr : \   <  %qced_plink_evec | tail -n+2 > qced_plink_pruned.pca.cov"
		},
		'do_logistic_pcs_1to10': {
			'command': plink + " --bfile %qced_plink_bfiles --maf %cut_maf --logistic hide-covar --ci %cut_ci --covar %qced_plink_cov --covar-number 1-10 --out qced_plink_top10pcs"
		},
		'do_logistic_top2pcs': {
			'command': plink + " --bfile %qced_plink_bfiles --maf %cut_maf --logistic hide-covar --ci %cut_ci --covar %qced_plink_cov --covar-number 1-2 --out qced_plink_top2pcs"
		},
		'generate_logistic_plot': {
			'command': "Rscript ../../logistic_plot.R"
		},
		'transform_plinkbfiles2tped': {
			'command': plink + " --bfile %qced_plink_bfiles --maf %cut_maf --recode12 --output-missing-genotype 0 --transpose --out qced_plink"
		},
		'replace_missingpheno_as_na': {
			'command': plink + " --bfile %qced_plink_bfiles --output-missing-phenotype 'NA' --make-just-fam --out qced_plink_missingpheno_as_NA"
		},
		'make_pheno_for_emmax': {
			'command': "cut -f 1-2,6 -d \  %qced_plink_fam_miss_as_NA > qced_plink.phe"
		},
		'create_kinship_matrix': {
			'command': "emmax-kin -v -d 10 qced_plink"
		},
		'do_emmax_assoc_testing': {
			'command': "emmax -v -d 10 -t qced_plink -p %emmax_pheno -k %emmax_kin_matrix -o qced_plink_emmax"
		},
		'sort_emmax_results': {
			'command': "sort -k4 -g %emmax_assoc_results > qced_plink_emmax_sorted.ps"
		},
	}
	
else:
    stages = {
		'do_permutation_testing': {
			'command': plink + " --bfile %qced_plink_bfiles --allow-no-sex --model mperm=10000 --out qced_plink_permtest"
		},
		'sort_permtest_results': {
			'command': "sort -k4n -r %permtest_results > qced_plink_permtest_sorted.model.best.mperm"
		},
		'convert_evec2cov': {
			'command': "tr : \   <  %qced_plink_evec | tail -n+2 > qced_plink_pruned.pca.cov"
		},
		'do_logistic_pcs_1to10': {
			'command': plink + " --bfile %qced_plink_bfiles --allow-no-sex --maf %cut_maf --logistic hide-covar --ci %cut_ci --covar %qced_plink_cov --covar-number 1-10 --out qced_plink_top10pcs"
		},
		'do_logistic_top2pcs': {
			'command': plink + " --bfile %qced_plink_bfiles --allow-no-sex --maf %cut_maf --logistic hide-covar --ci %cut_ci --covar %qced_plink_cov --covar-number 1-2 --out qced_plink_top2pcs"
		},
		'generate_logistic_plot': {
			'command': "Rscript ../../logistic_plot.R"
		},
		'transform_plinkbfiles2tped': {
			'command': plink + " --bfile %qced_plink_bfiles --allow-no-sex --maf %cut_maf --recode12 --output-missing-genotype 0 --transpose --out qced_plink"
		},
		'replace_missingpheno_as_na': {
			'command': plink + " --bfile %qced_plink_bfiles --allow-no-sex --output-missing-phenotype 'NA' --make-just-fam --out qced_plink_missingpheno_as_NA"
		},
		'make_pheno_for_emmax': {
			'command': "cut -f 1-2,6 -d \  %qced_plink_fam_miss_as_NA > qced_plink.phe"
		},
		'create_kinship_matrix': {
			'command': "emmax-kin -v -d 10 qced_plink"
		},
		'do_emmax_assoc_testing': {
			'command': "emmax -v -d 10 -t qced_plink -p %emmax_pheno -k %emmax_kin_matrix -o qced_plink_emmax"
		},
		'sort_emmax_results': {
			'command': "sort -k4 -g %emmax_assoc_results > qced_plink_emmax_sorted.ps"
		},
	}
