#!/bin/env python

""" pipeline_qcplink_tasks6-14of20.py 

    -pipeline for Sample and SNP qc using plink (version 1.9).
=============================================================================
"""

# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)


# user defined module imports
import Filemanager as FM

import WitsgwasSoftware as SW

import WitsgwasScripts as SC



# Shorthand access to options defined in pipeline_qcplink_config.py
#==========================================

working_files = pipeline_options.working_files
preselected_cutoff = pipeline_options.preselected_cutoff
logDir = pipeline_options.pipeline['logDir']



# Data setup process and input organisation
#==========================================

# assign the current project directory
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = working_files['current_dir'] 
print "Current project directory %s" % SC.CURRENT_PROJECT_DIR


global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"


# cd into the current project dir.
os.chdir(SC.CURRENT_PROJECT_DIR)

# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir

 

# Paths to working files and intermediate result files
#==========================================

# path to plink binary files resulting from removal of duplicates
qcplink_bfiles = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "qcplink")

# path to plink binary files resulting from sample QC
sample_qced_plink_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "clean_inds_qcplink")

# path to high-LD-regions.txt (source: Anderson, C. et al. Nature Protocols. 5, 1564-1573, 2010) 
high_LD_regions = os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, "high_LD_regions.txt")

# path to plots generated during plink QC
qcplink_plots = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "qcplink_plots") + '/') 



# Preselected user cutoffs
#==========================================

cut_het_high = preselected_cutoff['heterozygosity_cutoff_high']
cut_het_low = preselected_cutoff['heterozygosity_cutoff_low']
cut_miss = preselected_cutoff['missingness_cutoff']




# Print project information
#==========================================

print "Starting qcplink tasks6-14of20 for project: %s " % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print



# Pipeline declarations
#==========================================

# Phase 1 - Sample QC continued.


""" Task6: Identification of individuals with elevated missing data rates or outlying heterozygosity rate """

#Task6: qcplink_miss.imiss and qcplink_het.het -> fail_miss_het_qcplink.txt

@transform('qcplink.MissHetplot.Success',                                                  # Input             = qcplink
           suffix('.MissHetplot.Success'),                                                 # Input suffix      = .MissHetplot.Success
           '.FailMissHet.Success')                                                         # Output suffix     = .FailMissHet.Success
           
def find_indiv_with_high_miss_extreme_het(inputs, outputs):
    """
    Based on a preselected cutoff identify individuals with high missingness and/or 
    outlier heterozygosity. cutoffs should have been decided by looking at pairs.imiss-vs-het.pdf
    """
    
    flagFile = outputs
    print "Writing out individuals with high missingness and/or outlier heterozygosity"
    runStageCheck('find_indiv_with_high_miss_extreme_het', flagFile, cut_het_high, cut_het_low, cut_miss)
    print "Task 6 completed successfully \n"


    
""" Tasks 7.0-7.4: Identification of duplicated or related individuals """
    
#Task7.0: qcplink_bfiles -> qcplink_IBD.prune.in

@transform(find_indiv_with_high_miss_extreme_het,                                         # Input             = qcplink
           suffix('.FailMissHet.Success'),                                                # Input suffix      = .FailMissHet.Success
           add_inputs(high_LD_regions),                                                   # additional Inputs = high_LD_regions
           '.PruneForIBD.Success')                                                        # Output suffix     = .PruneForIBD.Success
           
def prune_for_IBD(inputs, outputs):
    """
    Minimize computational complexity prior to calculating IBD by pruning the SNPs; such that
    no pair of SNPs within a 50-SNP sliding window (advanced by 5 SNPs each time) has an
    r_squared value greater than 0.2
    """
    
    high_LD_regions = inputs[1]
    
    flagFile = outputs
    print ("pruning the SNPs such that no pair of SNPs within a 50-SNP sliding window. \n" 
    "(advanced by 5 SNPs each time) has an r_squared value greater than 0.2")
    runStageCheck('prune_for_IBD', flagFile, qcplink_bfiles, high_LD_regions)
    print "Task 7 completed successfully \n"
    
    
#Task7.1: qcplink_bfiles -> qcplink_IBD.genome

@transform(prune_for_IBD,                                                                 # Input             = qcplink
           suffix('.PruneForIBD.Success'),                                                # Input suffix      = .PruneForIBD.Success
           add_inputs("qcplink_IBD.prune.in"),                                            # additional Inputs = qcplink_IBD.prune.in
           '.CalculateIBD.Success')                                                       # Output suffix     = .CalculateIBD.Success
           
def calculate_IBD(inputs, outputs):
    """
    Calculate IBD to identify duplicated or related individuals. 
    """
    
    qcplink_IBD_prune = inputs[1]
    
    flagFile = outputs
    print "Calculating IBD to identify duplicated or related individuals."
    runStageCheck('calculate_IBD', flagFile, qcplink_bfiles, qcplink_IBD_prune)
    print "Task 7.1 completed successfully \n"
    
    
#Task7.2: qcplink_bfiles -> qcplink_IBD_min_0-04.genome

@transform(calculate_IBD,                                                                 # Input             = qcplink
           suffix('.CalculateIBD.Success'),                                               # Input suffix      = .CalculateIBD.Success
           add_inputs("qcplink_IBD.prune.in"),                                            # additional Inputs = qcplink_IBD.prune.in
           '.CalculateIBDminPiHat.Success')                                               # Output suffix     = .CalculateIBDminPiHat.Success
           
def calculate_IBD_min_piHat(inputs, outputs):
    """
    Use estimates of pairwise IBD to find pairs of individuals who look too similar to eachother, 
    i.e. more than we would expect by chance in a random sample. To reduce the file size,  
    we'll use the --minX option to only output to plink.genome pairs where PI_HAT is greater than X. 
    PI_HAT = sum of prob. that a pair share two alleles and a pair shares a single allele 
    i.e. P(IBD=2)+0.5*P(IBD=1) (basically, PI_Hat is the proportion of  IBD )
    """
    
    qcplink_IBD_prune = inputs[1]
    
    flagFile = outputs
    print "Calculating IBD to identify duplicated or related individuals with min piHat 0.04."
    runStageCheck('calculate_IBD_min_piHat', flagFile, qcplink_bfiles, qcplink_IBD_prune)
    print "Task 7.2 completed successfully \n"
    

#Task7.3: qcplink_IBD_min_0-04.genome -> qcplink_IBD_min_0-04_sorted_piHat.txt

@transform(calculate_IBD_min_piHat,                                                       # Input             = qcplink
           suffix('.CalculateIBDminPiHat.Success'),                                       # Input suffix      = .CalculateIBDminPiHat.Success
           add_inputs("qcplink_IBD_min_0-04.genome"),                                     # additional Inputs = qcplink_IBD_min_0-04.genome
           '.SortIBDminPiHat.Success')                                                    # Output suffix     = .SortIBDminPiHat.Success
           
def sort_by_piHat(inputs, outputs):
    """
    Sort qcplink_IBD_min_0-04.genome by piHat values.
    """
    
    qcplink_IBD_min = inputs[1]
    
    flagFile = outputs
    print "Sorting qcplink_IBD_min_0-04.genome by piHat values"
    runStageCheck('sort_by_piHat', flagFile, qcplink_IBD_min)
    print "Task 7.3 completed successfully \n"

    
#Task7.4: qcplink_miss.imiss and qcplink_IBD.genome -> fail_IBD_qcplink.txt

@follows(calculate_IBD)
@transform(sort_by_piHat,                                                                 # Input             = qcplink
           suffix('.SortIBDminPiHat.Success'),                                            # Input suffix      = .SortIBDminPiHat.Success
           add_inputs("qcplink_miss.imiss", "qcplink_IBD.genome"),                        # additional Inputs = qcplink_miss.imiss and qcplink_IBD.genome
           '.FilterRelatedIndiv.Success')                                                 # Output suffix     = .FilterRelatedIndiv.Success
           
def filter_related_indiv(inputs, outputs):
    """
    Identify all pairs of individuals with an IBD > 0.185. 
    """
    

    qcplink_imiss = os.path.splitext(inputs[1])[0]
    qcplink_genome = os.path.splitext(inputs[2])[0]
    
    flagFile = outputs
    print "Identifying all pairs of individuals with an IBD > 0.185."
    print "(IBD > 0.185 is halfway between the expected IBD for third- and second-degree relatives)"
    runStageCheck('filter_related_indiv', flagFile, qcplink_imiss, qcplink_genome)
    print "Task 7.4 completed successfully \n"



""" Tasks 8.0-8.1: Removal of all individuals failing Qc """
  
#Task8.0: (fail_sex_check_qcplink.txt, fail_miss_het_qcplink.txt) -> fail_qcplink_inds.txt

@transform(filter_related_indiv,                                                                              # Input             = qcplink
           suffix('.FilterRelatedIndiv.Success'),                                                             # Input suffix      = .FilterRelatedIndiv.Success
           add_inputs("fail_sex_check_qcplink.txt", "fail_miss_het_qcplink.txt"),                             # additional Inputs = fail_sex_check_qcplink.txt, fail_miss_het_qcplink.txt
           '.FailQcplinkInds.Success')                                                                        # Output suffix     = .FailQcplinkInds.Success
           
def join_qcplink_failed_indiv_into_singlefile(inputs, outputs):
    """
    Join all qcplink failed individuals into a single file.
    """
    fail_sexcheck, fail_misshet = inputs[1:]
    
    flagFile = outputs
    print "Joining all qcplink failed individuals into a single file."
    runStageCheck('join_qcplink_failed_indiv_into_singlefile', flagFile, fail_sexcheck, fail_misshet)
    print "Task 8.0 completed successfully \n"
    
       
#Task8.1: qcplink_bfiles and fail_qcplink_inds.txt -> clean_inds_qcplink.{fam, bim, bed}

@transform(join_qcplink_failed_indiv_into_singlefile,                                     # Input             = qcplink
           suffix('.FailQcplinkInds.Success'),                                            # Input suffix      = .FailQcplinkInds.Success
           add_inputs("fail_qcplink_inds.txt"),                                           # additional Inputs = fail_qcplink_inds.txt
           '.CleanIndsQcplink.Success')                                                   # Output suffix     = .CleanIndsQcplink.Success

def remove_qcplink_failed_indiv(inputs, outputs):
    """
    remove all qcplink failed individuals from plink binary files.
    """
    fail_qcplink = inputs[1]
    
    flagFile = outputs
    print "Removing all qcplink failed individuals from plink binary files"
    runStageCheck('remove_qcplink_failed_indiv', flagFile, qcplink_bfiles, fail_qcplink)
    print "Task 8.1 completed successfully \n"




# Phase 2 - SNP QC.



""" Tasks 9-10: Determining a minor allele frequency cutoff """

#Task9: sample_qced_plink_bfiles -> clean_inds_qcplink_freq.frq

@transform(remove_qcplink_failed_indiv,                                                   # Input             = qcplink
           suffix('.CleanIndsQcplink.Success'),                                           # Input suffix      = CleanIndsQcplink.Success
           '.MinorAlleleFreq.Success')                                                    # Output suffix     = .MinorAlleleFreq.Success

def calc_maf(inputs, outputs):
    """
    Calculate minor allele frequencies.
    """
        
    flagFile = outputs
    print "Calculating minor allele frequencies"
    runStageCheck('calc_maf', flagFile, sample_qced_plink_bfiles)
    print "Task 9 completed successfully \n"
    
    
#Task10: clean_inds_qcplink_freq.frq -> maf_plot.pdf

@transform(calc_maf,                                                                      # Input             = qcplink
           suffix('.MinorAlleleFreq.Success'),                                            # Input suffix      = MinorAlleleFreq.Success
           '.Mafplot.Success')                                                            # Output suffix     = .Mafplot.Success
           
def generate_maf_plot(inputs, outputs):
    """
    Plot the distribution of MAF values using the script maf_plot_qcplink.R.
    """
        
    flagFile = outputs
    print "Plotting the distribution of MAF values using the script maf_plot_qcplink.R"
    print "This plot should be used to decide a minor allele freq. cutoff"
    runStageCheck('generate_maf_plot', flagFile)
    print "Task 10 completed successfully \n"
    



""" Tasks 11-12: Identification of all markers with an excessive missing data rate """
    
#Task11: sample_qced_plink_bfiles -> clean_inds_qcplink_missing.lmiss

@transform(generate_maf_plot,                                                             # Input             = qcplink
           suffix('.Mafplot.Success'),                                                    # Input suffix      = .Mafplot.Success
           '.SnpMissingness.Success')                                                     # Output suffix     = .SnpMissingness.Success
           
def calc_SNP_missingness(inputs, outputs):
    """
    Calculate SNP missingness.
    """
    
    flagFile = outputs
    print "Calculating SNP missingness"
    runStageCheck('calc_SNP_missingness', flagFile, sample_qced_plink_bfiles)
    print "Task 11 completed successfully \n"
    

#Task12: clean_inds_qcplink_missing.lmiss -> snpmiss_plot.pdf

@transform(calc_SNP_missingness,                                                          # Input             = qcplink
           suffix('.SnpMissingness.Success'),                                             # Input suffix      = .SnpMissingness.Success
           '.SnpMissplot.Success')                                                        # Output suffix     = .SnpMissplot.Success
           
def generate_SNP_miss_plot(inputs, outputs):
    """
    Plot the distribution of missingness values using the script snpmiss_plot_qcplink.R.
    """
        
    flagFile = outputs
    print "Plotting the distribution of MAF values using the script snpmiss_plot_qcplink.R"
    print "This plot should be used to decide a SNP missingness cutoff"
    runStageCheck('generate_SNP_miss_plot', flagFile)
    print "Task 12 completed successfully \n"


    

""" Tasks 13-14: Test markers for different genotype call rates between cases and controls """

#Task13: sample_qced_plink_bfiles -> clean_inds_qcplink_test_missing.missing

@transform(generate_SNP_miss_plot,                                                        # Input             = qcplink
           suffix('.SnpMissplot.Success'),                                                # Input suffix      = .SnpMissplot.Success
           '.DifferentialMissingness.Success')                                            # Output suffix     = .DifferentialMissingness.Success
           
def calc_SNP_differential_missingness(inputs, outputs):
    """
    Calculate differential missingness.
    """
        
    flagFile = outputs
    print "Calculating differential missingness"
    runStageCheck('calc_SNP_differential_missingness', flagFile, sample_qced_plink_bfiles)
    print "Task 13 completed successfully \n"
    

#Task14: clean_inds_qcplink_test_missing.missing -> diffmiss_plot.pdf
    
@transform(calc_SNP_differential_missingness,                                             # Input             = qcplink
           suffix('.DifferentialMissingness.Success'),                                    # Input suffix      = .DifferentialMissingness.Success
           '.DiffMissplot.Success')                                                       # Output suffix     = .DiffMissplot.Success
           
def generate_diff_miss_plot(inputs, outputs):
    """
    Plot the distribution of differential missingness P-values using the script diffmiss_plot_qcplink.R.
    """
        
    flagFile = outputs
    print "Plotting the distribution of differential missingness P-values using the script diffmiss_plot_qcplink.R"
    print "This plot should be used to decide a differential missingness P-value cutoff"
    runStageCheck('generate_diff_miss_plot', flagFile)
    print "Task 14 completed successfully \n"















