#!/bin/env python

""" pipeline_qcplink_tasks15-20of20.py 

    -pipeline for Sample and SNP qc using plink (version 1.9).
=============================================================================
"""

# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)


# user defined module imports
import Filemanager as FM

import WitsgwasSoftware as SW

import WitsgwasScripts as SC




# Shorthand access to options defined in pipeline_qcplink_config.py
#==========================================

working_files = pipeline_options.working_files
preselected_cutoff = pipeline_options.preselected_cutoff
logDir = pipeline_options.pipeline['logDir']



# Data setup process and input organisation
#==========================================

# assign the current project directory
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = working_files['current_dir'] 
print "Current project directory %s" % SC.CURRENT_PROJECT_DIR


global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"


# cd into the current project dir.
os.chdir(SC.CURRENT_PROJECT_DIR)

# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir



# Paths to working files and intermediate result files
#==========================================

# path to plink binary files resulting from sample QC
sample_qced_plink_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "clean_inds_qcplink")

# path to plink binary files resulting from SNP QC
SNP_qced_plink_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "clean_qcplink")

# path to X chr SNPs i.e. xsnps.bim
x_SNPs_bim = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "xsnps.bim")


# path to plots generated during plink QC
qcplink_plots = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "qcplink_plots") + '/')



# Preselected user cutoffs
#==========================================

# SNP differential missingness cutoff
cut_diff_miss = preselected_cutoff['differential_missingness_cutoff']

# SNP Hardy Weinburg deviation cutoff
cut_hwe = preselected_cutoff['hwe_cutoff']

# SNP minor allele frequency cutoff
cut_maf = preselected_cutoff['maf_cutoff']

# SNP missingness cutoff
cut_geno = preselected_cutoff['geno_cutoff']




# Print project information
#==========================================

print "Starting qcplink tasks15-20of20 for project: %s " % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print



#Task15: clean_inds_qcplink_test_missing.missing -> fail_diffmiss_qcplink.txt
    
@transform("qcplink.DiffMissplot.Success",                                                # Input             = qcplink
           suffix('.DiffMissplot.Success'),                                               # Input suffix      = .DiffMissplot.Success
           '.FailDiffMiss.Success')                                                       # Output suffix     = .FailDiffMiss.Success
           
def find_SNPs_extreme_diff_miss(inputs, outputs):
    """
    Select SNPs showing extreme differential missingness.
    """
        
    flagFile = outputs
    print "Based on a preselected cutoff,selecting SNPs showing extreme differential missingness."
    print "cutoffs should have been decided by looking at diffmiss_plot.pdf"
    runStageCheck('find_SNPs_extreme_diff_miss', flagFile, cut_diff_miss)
    print "Task 15 completed successfully \n"
    
    
#Task16: sample_qced_plink_bfiles -> clean_inds_qcplink_hwe.hwe

@transform(find_SNPs_extreme_diff_miss,                                                   # Input             = qcplink
           suffix('.FailDiffMiss.Success'),                                               # Input suffix      = .FailDiffMiss.Success
           '.FindHWEdeviations.Success')                                                  # Output suffix     = .FindHWEdeviations.Success
           
def find_SNPs_extreme_HWE_deviations(inputs, outputs):
    """
    Identify SNPs with extreme HWE deviations.
    """
    
    flagFile = outputs
    print "Identifying SNPs with extreme HWE deviations."
    runStageCheck('find_SNPs_extreme_HWE_deviations', flagFile, sample_qced_plink_bfiles)
    print "Task 16 completed successfully \n"
    
    
#Task17: clean_inds_qcplink_hwe.hwe -> clean_inds_qcplink_hweu.hwe

@transform(find_SNPs_extreme_HWE_deviations,                                              # Input             = qcplink
           suffix('.FindHWEdeviations.Success'),                                          # Input suffix      = .FindHWEdeviations.Success
           '.FindUnaffectedForHWEplot.Success')                                           # Output suffix     = .FindUnaffectedForHWEplot.Success
           
def find_unaffected_for_HWE_plot(inputs, outputs):
    """
    Select unaffected only from clean_inds_qcplink_hwe.hwe for HWE Plot.
    """
    
    flagFile = outputs
    print "Selecting unaffected only from clean_inds_qcplink_hwe.hwe for HWE Plot."
    runStageCheck('find_unaffected_for_HWE_plot', flagFile)
    print "Task 17 completed successfully \n"
    
    
#Task18: clean_inds_qcplink_hweu.hwe -> hwe_plot.pdf
    
@transform(find_unaffected_for_HWE_plot,                                                  # Input             = qcplink
           suffix('.FindUnaffectedForHWEplot.Success'),                                   # Input suffix      = .FindUnaffectedForHWEplot.Success
           '.hweplot.Success')                                                            # Output suffix     = .hweplot.Success
           
def generate_hwe_plot(inputs, outputs):
    """
    Plot the distribution of HWE P-values(in controls) using the script hwe_plot_qcplink.R.
    """
        
    flagFile = outputs
    print "Plotting the distribution of HWE P-values(in controls) using the script hwe_plot_qcplink.R"
    print "This plot should be used to decide an HWE P-value cutoff"
    runStageCheck('generate_hwe_plot', flagFile)
    print "Task 18 completed successfully \n"
    
    
#Task19: sample_qced_plink_bfiles and fail_diffmiss_qcplink.txt  -> clean_qcplink.{fam, bim, bed}

@transform(generate_hwe_plot,                                                             # Input             = qcplink
           suffix('.hweplot.Success'),                                                    # Input suffix      = .hweplot.Success
           add_inputs('fail_diffmiss_qcplink.txt'),                                       # additional Inputs = fail_diffmiss_qcplink.txt
           '.RemoveSNPsFailingQc.Success')                                                # Output suffix     = .RemoveSNPsFailingQc.Success
           
def remove_SNPs_failing_Qc(inputs, outputs):
    """
    Remove SNPs failing QC.
    """
    
    fail_diffmiss = inputs[1]
    
    flagFile = outputs
    print "Removing SNPs failing QC"
    runStageCheck('remove_SNPs_failing_Qc', flagFile, sample_qced_plink_bfiles, cut_maf, cut_geno, fail_diffmiss, cut_hwe)
    print "Task 19 completed successfully \n"
    
    
#Task20.0: SNP_qced_plink_bfiles  -> xsnps.{fam, bim, bed}

@transform(remove_SNPs_failing_Qc,                                                        # Input             = qcplink
           suffix('.RemoveSNPsFailingQc.Success'),                                        # Input suffix      = .RemoveSNPsFailingQc.Success
           '.FindXchrSNPs.Success')                                                       # Output suffix     = .FindXchrSNPs.Success
           
def find_xchr_SNPs(inputs, outputs):
    """
    Write out X chr SNPs to xsnps.{fam, bim, bed}.
    """
    
    flagFile = outputs
    print "Writing out X chr SNPs to xsnps.{fam, bim, bed}"
    runStageCheck('find_xchr_SNPs', flagFile, SNP_qced_plink_bfiles)
    print "Task 20.0 completed successfully \n"
    
    
#Task20.1: SNP_qced_plink_bfiles and xsnps.bim -> qced_plink.{fam, bim, bed}

@transform(find_xchr_SNPs,                                                                # Input             = qcplink
           suffix('.FindXchrSNPs.Success'),                                               # Input suffix      = .FindXchrSNPs.Success
           '.RemoveXchrSNPs.Success')                                                     # Output suffix     = .RemoveXchrSNPs
           
def remove_xchr_SNPs(inputs, outputs):
    """
    Remove X chr SNPs.
    """
    
    flagFile = outputs
    print "Removing X chr SNPs"
    runStageCheck('remove_xchr_SNPs', flagFile, SNP_qced_plink_bfiles, x_SNPs_bim)
    print "Task 20.1 completed successfully \n"

