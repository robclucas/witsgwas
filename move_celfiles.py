#!/bin/env python

"""
Function to move .CEL files that fail SNP chip qc to a separate folder
Inputs: -f celfiles.genoqcfailed -origdir celfiles_dir -destdir failed_genoqc_cell_files 
"""
import os
import subprocess
import argparse

def move_cel_files(file, path2origdir, path2destdir):

    # read lines in celfiles.genoqcfailed into a list called failed_affygenoqc_files
    with open(file, 'r') as outfile:
        failed_affygenoqc_files = outfile.readlines() 
        
    for item in failed_affygenoqc_files:
        # ignoring commment lines
        if (item.startswith('#') or item.startswith('\n')):
            pass
        else:
            # splitting on the tab character, extract first field (contains celfile name)
            item = item.split("\t")
            print item
            failed_celfile_name = item[0].strip()
            print failed_celfile_name
            path_failed_celfile = os.path.join(path2origdir, failed_celfile_name)
            print path_failed_celfile
            #subprocess.call("mv %s %s" % (path_failed_celfile, path2destdir), shell=True)


#----------------------------------------------------------------------

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Options for move_celfiles.py', version='%(prog)s 1.0')

    parser.add_argument('-f', '--file', action='store', dest='filename',
					help='Stores the filename for the file of interest ')
					
    parser.add_argument('-od', '--origdir', action='store', dest='path2origdir',
				    help='Stores the path to the celfiles_dir: ')
					
    parser.add_argument('-dd', '--destdir', action='store', dest='path2destdir',
					help='Stores the path to the dir: of interest ')

					
					
    arguments = parser.parse_args()
    
    move_cel_files(arguments.filename, arguments.path2origdir, arguments.path2destdir)


    	


