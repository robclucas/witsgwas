'''
   Software config file: paths to software used in the witsGWAS code collection

   '''
import os

bioinf = "/opt/exp_soft/bioinf/bin/"

apt = "/opt/exp_soft/bioinf/apt/bin/"

exp_soft = "/opt/exp_soft/"

usr_bin = "/usr/bin/"

python_ver = "python27/bin/"


# modules


# Admixture ver: 
admixture = os.path.join(bioinf, "admixture")

# Affymetrix Power Tools
aptgenoqc = os.path.join(apt, "apt-geno-qc")
aptgenoprobeset = os.path.join(apt, "apt-probeset-genotype")


# Emmax ver:
emmax = os.path.join(bioinf, "emmax")

# eigenstrat
eigenstrat = os.path.join(bioinf, "eigenstrat")

# gcta ver:

# plink ver: PLINK v1.90b3i
plink = os.path.join(bioinf, "plink")

# plink ver: PLINK v1.07
plink1 = os.path.join(bioinf, "plink1")

# perl ver:
perl = os.path.join(usr_bin, "perl")

# primus ver:

# python ver: 2.7.2
python = os.path.join(exp_soft, python_ver, "python")

# R ver: 3.2.1
R = os.path.join(usr_bin, "R")

# Rubra ver: rubra 0.1.5
rubra = os.path.join(exp_soft, python_ver, "rubra")

# Ruffus ver:

# runpca ver: 1.21
runpca = os.path.join(bioinf, "runpca")







 

 
 
 
