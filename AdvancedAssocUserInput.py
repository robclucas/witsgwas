#!/bin/env python

""" AdvancedAssocUserInput.py 

    -Configuration file for the user to supply the projectname, author,  
    maf and confidence interval cutoffs specific to pipeline_advanced_assoc_testing.py
=============================================================================
"""

# settings for pipeline_advanced_assoc_testing.py
#==========================================

projectname = 'E-MTAB-3729'
author = 'Magosi'

# Availability of sex info
sexinfo_available = True

# path to plink binary files
qced_plink_bfiles = '/opt/exp_soft/bioinf/witsGWAS/projects/E-MTAB-3729-qcplink-Magosi-2015-09-09_17-27-52/qced_plink'

# path to .evec files
qced_plink_evec = '/opt/exp_soft/bioinf/witsGWAS/projects/E-MTAB-3729-qcplink-Magosi-2015-09-09_17-27-52/qced_plink_pruned.pca.evec'



# minor allele frequency cutoffs: Standard cutoff (0.01) cut_het_high = 0.30
maf_cutoff = 0.01

# confidence interval cutoff: Standard cutoff (0.95) 
conf_int_cutoff = 0.95

