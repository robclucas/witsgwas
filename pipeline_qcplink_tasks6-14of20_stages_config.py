#!/bin/env python

""" pipeline_qcplink_tasks6-14of20_stages_config.py

    -Configuration file to set options specific to each stage/task in pipeline_qcplink_tasks6-14of20.py
=============================================================================
"""
import os

import PlinkUserInput as I

import WitsgwasSoftware as SW

python = SW.python
plink = SW.plink
plink1 = SW.plink1
perl = SW.perl
R = SW.R



# stageDefaults contains the default options which are applied to each stage (command).
# This section is required for every Rubra pipeline.
# These can be overridden by options defined for individual stages, below.
# Stage options which Rubra will recognise are: 
#  - distributed: a boolean determining whether the task should be submitted to a cluster
#      job scheduling system (True) or run on the system local to Rubra (False). 
#  - walltime: for a distributed PBS job, gives the walltime requested from the job
#      queue system; the maximum allowed runtime. For local jobs has no effect.
#  - memInGB: for a distributed PBS job, gives the memory in Gigabytes requested from the 
#      job queue system. For local jobs has no effect.
#  - queue: for a distributed PBS job, this is the name of the queue to submit the
#      job to. For local jobs has no effect. This is currently a mandatory field for
#      distributed jobs, but can be set to None.
#  - modules: the modules to be loaded before running the task. This is intended for  
#      systems with environment modules installed. Rubra will call module load on each 
#      required module before running the task. Note that defining modules for individual 
#      stages will override (not add to) any modules listed here. This currently only
#      works for distributed jobs.



stageDefaults = {
    'distributed': True,
    'queue': 'WitsLong',
    'walltime': "6:00:00",
    'memInGB': 16,
    'name': None,
    'modules': [
#         python,
#         plink,
#         perl,
#         R,
          'gwaspipe',
    ]
}




# stages should hold the details of each stage which can be called by runStageCheck.
# This section is required for every Rubra pipeline.
# Calling a stage in this way carries out checkpointing and, if desired, batch job
# submission. 
# Each stage must contain a 'command' definition. See stageDefaults above for other 
# allowable options.

if I.sexinfo_available:
	stages = {
		'find_indiv_with_high_miss_extreme_het': {
			"command": perl + " ../../select_miss_het_qcplink.pl %cut_het_high %cut_het_low %cut_miss"
		},
		'prune_for_IBD': {
			"command": plink + " --bfile %qcplink_bfiles --exclude %high_LD_regions --range --indep-pairwise 50 5 0.2 --out qcplink_IBD"
		},
		'calculate_IBD': {
			"command": plink + " --bfile %qcplink_bfiles --extract %qcplink_IBD_prune --genome --out qcplink_IBD"
		},
		'calculate_IBD_min_piHat': {
			"command": plink + " --bfile %qcplink_bfiles --extract %qcplink_IBD_prune --genome --min 0.04 --out qcplink_IBD_min_0-04"
		},
		'sort_by_piHat': {
			"command": "sort -k10n %qcplink_IBD_min > qcplink_IBD_min_0-04_sorted_piHat.txt"
		},
		'filter_related_indiv': {
			"command": perl + " ../../run_IBD_QC_qcplink.pl %qcplink_imiss %qcplink_genome"
		},
		'join_qcplink_failed_indiv_into_singlefile': {
			'command': "cat %fail_sexcheck %fail_misshet | sort -k1 | uniq > fail_qcplink_inds.txt"
		},
		'remove_qcplink_failed_indiv': {
			'command': plink + " --bfile %qcplink_bfiles --remove %fail_qcplink --make-bed --out clean_inds_qcplink"
		},
		'calc_maf': {
			'command': plink + " --bfile %sample_qced_plink_bfiles --freq --out clean_inds_qcplink_freq"
		},
		'generate_maf_plot': {
			'command': "Rscript ../../maf_plot_qcplink.R"
		},
		'calc_SNP_missingness': {
			'command': plink + " --bfile %sample_qced_plink_bfiles --missing --out clean_inds_qcplink_missing"
		},
		'generate_SNP_miss_plot': {
			'command': "Rscript ../../snpmiss_plot_qcplink.R"
		},
		'calc_SNP_differential_missingness': {
			'command': plink + " --bfile %sample_qced_plink_bfiles --test-missing --out clean_inds_qcplink_test_missing"
		},
		'generate_diff_miss_plot': {
			'command': "Rscript ../../diffmiss_plot_qcplink.R"
		},
	}
	
else:
	stages = {
		'find_indiv_with_high_miss_extreme_het': {
			"command": perl + " ../../select_miss_het_qcplink.pl %cut_het_high %cut_het_low %cut_miss"
		},
		'prune_for_IBD': {
			"command": plink + " --noweb --bfile %qcplink_bfiles --allow-no-sex --exclude %high_LD_regions --range --indep-pairwise 50 5 0.2 --out qcplink_IBD"
		},
		'calculate_IBD': {
			"command": plink + " --noweb --bfile %qcplink_bfiles --allow-no-sex --extract %qcplink_IBD_prune --genome --out qcplink_IBD"
		},
		'calculate_IBD_min_piHat': {
			"command": plink + " --noweb --bfile %qcplink_bfiles --allow-no-sex --extract %qcplink_IBD_prune --genome --min 0.04 --out qcplink_IBD_min_0-04"
		},
		'sort_by_piHat': {
			"command": "sort -k10n %qcplink_IBD_min > qcplink_IBD_min_0-04_sorted_piHat.txt"
		},
		'filter_related_indiv': {
			"command": perl + " ../../run_IBD_QC_qcplink.pl %qcplink_imiss %qcplink_genome"
		},
		'join_qcplink_failed_indiv_into_singlefile': {
			'command': "cat %fail_sexcheck %fail_misshet | sort -k1 | uniq > fail_qcplink_inds.txt"
		},
		'remove_qcplink_failed_indiv': {
			'command': plink + " --noweb --bfile %qcplink_bfiles --allow-no-sex --remove %fail_qcplink --make-bed --out clean_inds_qcplink"
		},
		'calc_maf': {
			'command': plink + " --noweb --bfile %sample_qced_plink_bfiles --allow-no-sex --freq --out clean_inds_qcplink_freq"
		},
		'generate_maf_plot': {
			'command': "Rscript ../../maf_plot_qcplink.R"
		},
		'calc_SNP_missingness': {
			'command': plink + " --noweb --bfile %sample_qced_plink_bfiles --allow-no-sex --missing --out clean_inds_qcplink_missing"
		},
		'generate_SNP_miss_plot': {
			'command': "Rscript ../../snpmiss_plot_qcplink.R"
		},
		'calc_SNP_differential_missingness': {
			'command': plink + " --bfile %sample_qced_plink_bfiles --allow-no-sex --test-missing --out clean_inds_qcplink_test_missing"
		},
		'generate_diff_miss_plot': {
			'command': "Rscript ../../diffmiss_plot_qcplink.R"
		},
	}
