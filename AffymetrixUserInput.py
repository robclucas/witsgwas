#!/bin/env python

""" AffymetrixUserInput.py 

    -Configuration file for the user to supply the projectname, author,  
    paths for the .CEL files dir and affymetrix libraries specific to 
    pipeline_qcaffymetrix.py
=============================================================================
"""


celfiles_dir = '/global/H3A_GWAS_Accreditation/snp-snp6/'
projectname = 'h3data_phase1'
author = 'Magosi'
affy_qc_lib =  '/global/AffyData/GenomeWideSNP_6_LibFiles/'
affy_annotation = '/global/AffyData/'
plink_phenotypes_cases = ''
    
