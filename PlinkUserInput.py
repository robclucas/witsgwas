#!/bin/env python

""" PlinkUserInput.py 

    -Configuration file for the user to supply the projectname, author,  
    missingness and heterozygosity cutoffs specific to 
    pipeline_qcplink.py
=============================================================================
"""

# settings for pipeline_qcplink_tasks1-5of20.py
#==========================================

projectname = 'E-MTAB-3729'
author = 'Magosi'

# Availability of sex info
sexinfo_available = True

# path to plink binary files
plink_binary_files = '/gwas/plinkqc/datasets'

# settings for pipeline_qcplink_tasks6-14of20.py
#==========================================

'''note: update PlinkUserInput.py with these settings *after* running pipeline_qcplink_tasks1-5of20.py '''

# path to the dir holding results from running pipeline_qcplink_tasks1-5of20.py
current_dir = '/opt/exp_soft/bioinf/witsGWAS/projects/E-MTAB-3729-qcplink-Magosi-2015-11-11_13-32-48/'


# heterozygosity cutoffs: Standard cutoff (+/- 3 sd from heterozygosity mean) or 
# choose on the basis of miss_het_plot.pdf
cut_het_high = 0.343
cut_het_low = 0.254

# individual missingness value cutoff: Standard cutoff (3% to 7%) or 
# choose on the basis of miss_het_plot.pdf
cut_miss = 0.05






# settings for pipeline_qcplink_tasks15-20of20.py
#==========================================

'''note: update PlinkUserInput.py with these settings *after* running pipeline_qcplink_tasks6-14of20.py '''

# SNP missingness value cutoff: Standard cutoff (>0.05) or 
# choose on the basis of snp_miss_plot.pdf
cut_geno = 0.05

# differential missingness cutoff: Use standard cutoff (0.00001) or choose on the basis of diffmiss_plot.pdf
cut_diff_miss = 0.00001

# HWE P-value cutoff: Use standard cutoff (0.00001) or choose on the basis of hwe_plot.pdf
cut_hwe = 0.00001

# maf cutoff: Use standard cutoff (MAF>0.01) or choose on the basis of maf_plot.pdf
cut_maf = 0.01



    
