#!/bin/env python

""" pipeline_qcaffymetrix_stages_config.py

    -Configuration file to set options specific to each stage/task in pipeline_qcaffymetrix.py
=============================================================================
"""
import os

import WitsgwasSoftware as SW

python = SW.python
plink = SW.plink
perl = SW.perl
R = SW.R
aptgenoqc = SW.aptgenoqc
aptgenoprobeset = SW.aptgenoprobeset



# stageDefaults contains the default options which are applied to each stage (command).
# This section is required for every Rubra pipeline.
# These can be overridden by options defined for individual stages, below.
# Stage options which Rubra will recognise are: 
#  - distributed: a boolean determining whether the task should be submitted to a cluster
#      job scheduling system (True) or run on the system local to Rubra (False). 
#  - walltime: for a distributed PBS job, gives the walltime requested from the job
#      queue system; the maximum allowed runtime. For local jobs has no effect.
#  - memInGB: for a distributed PBS job, gives the memory in Gigabytes requested from the 
#      job queue system. For local jobs has no effect.
#  - queue: for a distributed PBS job, this is the name of the queue to submit the
#      job to. For local jobs has no effect. This is currently a mandatory field for
#      distributed jobs, but can be set to None.
#  - modules: the modules to be loaded before running the task. This is intended for  
#      systems with environment modules installed. Rubra will call module load on each 
#      required module before running the task. Note that defining modules for individual 
#      stages will override (not add to) any modules listed here. This currently only
#      works for distributed jobs.

stageDefaults = {
    'distributed': True,
    'queue': 'WitsLong',
    'walltime': "6:00:00",
    'memInGB': 16,
    'name': None,
    'modules': [
#         aptgenoqc,
#         aptgenoprobeset,
#         python,
#         plink,
#         perl,
#         R,
          'gwaspipe'
    ]
}




# stages should hold the details of each stage which can be called by runStageCheck.
# This section is required for every Rubra pipeline.
# Calling a stage in this way carries out checkpointing and, if desired, batch job
# submission. 
# Each stage must contain a 'command' definition. See stageDefaults above for other 
# allowable options.
stages = {
    "do_affygenoqc": {
        "command": aptgenoqc + " --cdf-file %cdf --qcc-file %qcc --qca-file %qca --out-file %output --cel-files %celfiles"
    },
    'sort_affygenoqc_byQCR': {
        'command': "sort -k2n %genoqcresults > %genoqcresults_sortedbyQCR"
    },
    'sort_affygenoqc_byCQC': {
        'command': "sort -k8n %genoqcresults > %genoqcresults_sortedbyCQC"
    },
    'find_failed_affygenoqc': {
        'command': "awk -F '\t' '$2<0.86 || $8<0.4' %genoqcresults > %genoqcfailed"
    },
    'filter_passed_affygenoqc': {
        'command': "awk -F '\t' '$2>=0.86 && $8>=0.4' %genoqcresults > %genoqcfiltered"
    },
    'find_mean_cqc': {
        'command': python + " ../../meanCQC.py -f %genoqcfiltered -cn 8 -o %output"
    },
    'extract_namesof_genoqcfiltered': {
        'command': "awk '{ print $1 }' %genoqcfiltered > %namesgenoqcfiltered"
    },
    'extract_paths2genoqcfiltered': {
        'command': "grep -f %namesgenoqcfiltered %celfiles > %path2genoqcfiltered"
    },
    'do_affygenoprobeset': {
        'command': aptgenoprobeset + " -o %output_dir -c %cdf --set-gender-method cn-probe-chrXY-ratio --chrX-probes %xpb --chrY-probes %ypb --special-snps %ss --read-models-birdseed %mdl -a birdseed-v2 --cel-files %path2genoqcfiltered"
    },
    'sort_birdseedreport_byCR': {
        'command': "sort -k3n %birdseedreport > %output"
    },
    'find_failed_birdseedreport': {
        'command': "awk -F '\t' '$3<97' %birdseedreportsortedCR > %output"
    },
    'filter_passed_affygenoprobeset': {
        'command': "awk -F '\t' '$3>=97' %birdseedreportsortedCR > %output"
    },
    'extract_namesof_genoprobesetfiltered': {
        'command': "awk '{ print $1 }' %birdseedreportfiltered > %namesbirdseedreportfiltered"
    },
    'extract_paths2birdseedreportfiltered': {
        'command': "grep -f %namesbirdseedreportfiltered %celfiles > %path2birdseedreportfiltered"
    },
    'do_affygenoprobeset_2nd_pass': {
        'command': aptgenoprobeset + " -o %output_dir -c %cdf --set-gender-method cn-probe-chrXY-ratio --chrX-probes %xpb --chrY-probes %ypb --special-snps %ss --read-models-birdseed %mdl -a birdseed-v2 --cel-files %path2birdseedreportfiltered"
    },
    'birdseed2plink': {
        'command': perl + " ../../pedfilemaker_2015.pl -affy_calls %birdseedcalls -report %birdseedreport -annot %annot -out_dir %output"
    },
    'copy_mapfile': {
        'command': "cp %mapfile %output"
    },
    'copy_pedfile': {
        'command': "cp %pedfile %output"
    },
    'rename_celfiles_map_as_celfiles_preaddrsidmap': {
        'command': "mv %celfiles_map %output"
    },
    'add_dummy_rsids': {
        'command': python + " ../../add_rs_id_2015.py"
    },
    'make_celfiles_binary': {
        'command': plink + " --file %input_basename --make-bed --out %output_basename"
    },
    'make_pheno_cases_file': {
        'command': "grep -f %case_list %celfiles_fam > %output"
    },
    'update_plink_phenotypes': {
        'command': plink + " --bfile %input_basename --make-pheno %phenocases '*' --make-bed --out %output_basename"
    },
}
