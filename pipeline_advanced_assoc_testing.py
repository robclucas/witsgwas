#!/bin/env python

""" pipeline_advanced_assoc_testing.py 

    -pipeline for performing GWAS association analysis in PLINK (versions: 1.07 and 1.9).
=============================================================================


Authors: Lerato Magosi, Scott Hazelhurst.


This program implements an association workflow for human GWAS analysis using PLINK binary files.


Association analysis tasks include:
1. Permutation testing
2. Logistic regression with covariates from pca
3. Emmax - assoc testing

Furthermore, it (pipeline_advanced_assoc_testing.py) provides additional association
testing features to the basic association and CMH testing available in pipeline_assoc_testing.py.


Assumptions:
Pipeline assumes the following QC steps have been carried out:

Sample QC checking for: 
1. discordant sex information
2. missingness 
3. heterozygosity scores.
4. relatedness (IBD calculations)

SNP OC checking for:
1. minor allele frequencies 
2. SNP missingness 
3. differential missingness
4. Hardy Weinberg Equilibrium deviations.

Divergent Ancestry:
1. PCA to Exclude individuals showing divergent ancestry


Task management:
It employs Rubra for sending jobs to a linux cluster via PBS Torque (version 2.5). 
Rubra is a pipeline system for bioinformatics workflows that is built on top
of the Ruffus (http://www.ruffus.org.uk/) Python library (Ruffus version 2.2). 
Rubra adds support for running pipeline stages on a distributed computer cluster 
(https://github.com/bjpop/rubra) and also supports parallel evaluation of independent 
pipeline stages. (Rubra version 0.1.5)

The pipeline is configured by an options file in a python file,
including the actual commands which are run at each stage.


"""



# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)

# witsGWAS banner
from pyfiglet import Figlet

# user defined module imports
import Filemanager as FM
import WitsgwasSoftware as SW
import WitsgwasScripts as SC





# Shorthand access to options defined in pipeline_advanced_assoc_config.py
#==========================================

working_files = pipeline_options.working_files
preselected_cutoff = pipeline_options.preselected_cutoff
logDir = pipeline_options.pipeline['logDir']




# Data setup process and input organisation
#==========================================

f = Figlet(font='standard')
print f.renderText('witsGWAS')
print "(C) 2015 Lerato E. Magosi, Scott Hazelhurst"
print "http://magosil86.github.io/witsGWAS/"    
print "witsGWAS v0.1.0 is licensed under the MIT license. See LICENSE.txt"
print "----------------------------------------------------------------"


# create a directory for the current project
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = (os.path.join(SC.witsGWAS_PROJECTS_DIR, working_files['projectname']) + 
	'-adv_assoc-' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '/')

print "Current project directory %s" % SC.CURRENT_PROJECT_DIR

FM.create_dir(SC.CURRENT_PROJECT_DIR)


# path to the witsGWAS directory
global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"


# cd into the current project dir.
os.chdir(SC.CURRENT_PROJECT_DIR)


# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir

 
# create a dir. for storing plots
adv_assoc_plots = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "adv_assoc_plots") + '/') 
FM.create_dir(adv_assoc_plots)





# Paths to intermediate result files
#==========================================

qced_plink_bfiles = working_files['qced_plink_bfiles']
qced_plink_evec = working_files['qced_plink_evec']



# Preselected user cutoffs
#==========================================

# SNP minor allele frequency cutoff
cut_maf = preselected_cutoff['maf_cutoff']

# confidence interval cutoff
cut_ci = preselected_cutoff['conf_int_cutoff']




# Task switches: Booleans that specify the association methods to run
#==========================================




# Print project information
#==========================================

print "Starting project %s" % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print



# Pipeline declarations
#==========================================

# create a flagfile to start the pipeline as well as permutation association testing
FM.create_emptyfile('adv_assoc.Start')




# Phase 1 - Permutation testing.


""" Tasks1.0-1.1: Perform the max(T) permutation procedure """


#Task1.0: qced_plink_bfile  -> qced_plink_permtest.model.best.mperm

@transform('adv_assoc.Start',                                       # Input prefix      = adv_assoc
           suffix('.Start'),                                        # Input suffix      = .Start
           '.Permtest.Success')                                     # Output suffix     = .Permtest.Success 
           
def do_permutation_testing(inputs, outputs):
    """
    Permutation testing allows for the control of multiple testing and potential sub-structure.
    Permutation testing in samples of unrelated individuals, simply swaps the labels
    (assuming that individuals are interchangeable under the null) to provide a new dataset
    sampled under the null hypothesis. Note that only the phenotype-genotype relationship 
    is destroyed by permutation: the patterns of LD between SNPs will remain the same under 
    the observed and permuted samples.
    """
    
    flagFile = outputs
    print "Performing permutation testing to control for multiple testing and potential sub -structure"
    runStageCheck('do_permutation_testing', flagFile, qced_plink_bfiles)
    print "Task 1.0 completed successfully \n"



#Task1.1: qced_plink_permtest.model.best.mperm  -> qced_plink_permtest_sorted.model.best.mperm

@transform(do_permutation_testing,                                  # Input prefix      = adv_assoc
           suffix('.Permtest.Success'),                             # Input suffix      = .Permtest.Success
           add_inputs("qced_plink_permtest.model.best.mperm"),      # add_inputs        = qced_plink_permtest.model.best.mperm
           '.SortPermtest.Success')                                 # Output suffix     = .SortPermtest.Success 
           
def sort_permtest_results(inputs, outputs):
    """
    Sort the results from permutation testing on the 4th column
    """
    
    permtest_results = inputs[1]
    
    flagFile = outputs
    print "Sorting the results from permutation testing on the 4th column."
    runStageCheck('sort_permtest_results', flagFile, permtest_results)
    print "Task 1.1 completed successfully \n"
    
    

# Phase 2 - Logistic regression.


""" Tasks2.0-2.3: Logistic regression with covariates from pca """


    
#Task2.0: qced_plink_pruned.pca.evec -> qced_plink_pruned.pca.cov

@follows(do_permutation_testing)
@transform(sort_permtest_results,                                   # Input prefix      = adv_assoc
           suffix('.SortPermtest.Success'),                         # Input suffix      = .SortPermtest.Success
           '.Evec2Cov.Success')                                     # Output suffix     = .Evec2Cov.Success 
           
def convert_evec2cov(inputs, outputs):
    """
    Convert the .evec file to a .cov. The .cov file is just the .evec file without 
    the ":" between familyID and IndividualID. Will also remove the first two lines.
    """
    
    flagFile = outputs
    print "Converting the .evec file to a .cov"
    runStageCheck('convert_evec2cov', flagFile, qced_plink_evec)
    print "Task 2.0 completed successfully \n"

    
    
#Task2.1: qced_plink.{fam, bim, bed} + qced_plink_pruned.pca.cov -> qced_plink_top10pcs.assoc.logistic

@transform(convert_evec2cov,                                        # Input prefix      = adv_assoc
           suffix('.Evec2Cov.Success'),                             # Input suffix      = .Evec2Cov.Success
           add_inputs("qced_plink_pruned.pca.cov"),                 # add_inputs        = qced_plink_pruned.pca.cov
           '.dologisticPcs1to10.Success')                           # Output suffix     = .dologisticPcs1to20.Success 
           
def do_logistic_pcs_1to10(inputs, outputs):
    """
    Logistic regression using covariates 1-10 from pca at maf cutoff and confidence 
    interval pre-selected by the user.
    """
    
    qced_plink_cov = inputs[1]
    
    flagFile = outputs
    print "Doing logistic regression using covariates 1-10 from pca at maf cutoff preselected by user"
    runStageCheck('do_logistic_pcs_1to10', flagFile, qced_plink_bfiles, cut_maf, cut_ci, qced_plink_cov)
    print "Task 2.1 completed successfully \n"
    
    
    
#Task2.2: qced_plink.{fam, bim, bed} + qced_plink_pruned.pca.cov -> qced_plink_top2pcs.assoc.logistic

@transform(do_logistic_pcs_1to10,                                   # Input prefix      = adv_assoc
           suffix('.dologisticPcs1to10.Success'),                   # Input suffix      = .dologisticPcs1to10.Success
           add_inputs("qced_plink_pruned.pca.cov"),                 # add_inputs        = qced_plink_pruned.pca.cov
           '.dologisticTop2pcs.Success')                            # Output suffix     = .dologisticTop2pcs.Success
           
def do_logistic_top2pcs(inputs, outputs):
    """
    Logistic regression using top 2 most significant pcs (according to: 
    runpca  logfile) as covariates (maf cutoff and confidence interval pre-selected by the 
    user) to correct for population structure in GWAS. It should be noted that top PCs do 
    not always reflect population structure: they may reflect family relatedness, 
    long-range LD (for example, due to inversion polymorphisms), or assay artifacts 
    (Price et al, 2010).
    """
    
    qced_plink_cov = inputs[1]
    
    flagFile = outputs
    print ("Doing logistic regression using top 2 covariates from pca at maf cutoff and \n"
    "confidence interval pre-selected by the user")
    runStageCheck('do_logistic_top2pcs', flagFile, qced_plink_bfiles, cut_maf, cut_ci, qced_plink_cov)
    print "Task 2.2 completed successfully \n"
    

    
#Task2.3: qced_plink.assoc.logistic -> logistic_10pcs_plot.pdf
 
@transform(do_logistic_top2pcs,                                     # Input prefix      = adv_assoc
           suffix('.dologisticTop2pcs.Success'),                    # Input suffix      = .dologisticTop2pcs.Success
           '.LogisticPlot.Success')                                 # Output suffix     = .LogisticPlot.Success
           
def generate_logistic_plot(inputs, outputs):
    """
    Generate plot to visualize the logistic association results.
    """
    
    
    flagFile = outputs
    print "Generating plot to visualize the logistic association results"
    runStageCheck('generate_logistic_plot', flagFile)
    print "Task 2.3 completed successfully \n"
    
    

# Phase 3 - Emmax association testing.


""" Tasks3.0-3.5: Emmax association testing  """



#Task3.0: qced_plink.{fam, bim, bed} -> qced_plink.{tped, tfam}

@transform(generate_logistic_plot,                                  # Input prefix      = adv_assoc
           suffix('.LogisticPlot.Success'),                         # Input suffix      = .LogisticPlot.Success
           '.Transform2TPed.Success')                               # Output suffix     = .Transform2TPed.Success
           
def transform_plinkbfiles2tped(inputs, outputs):
    """
    Transform PLINK binaries to .tped and .tfam in preparation for Emmax association testing.
    """
    
    
    flagFile = outputs
    print "Transforming PLINK binaries to .tped and .tfam in preparation for Emmax association testing"
    runStageCheck('transform_plinkbfiles2tped', flagFile, qced_plink_bfiles, cut_maf)
    print "Task 3.0 completed successfully \n"
    

    
#Task3.1: qced_plink.{fam, bim, bed} -> qced_plink_missingpheno_as_NA.fam

@transform(transform_plinkbfiles2tped,                              # Input prefix      = adv_assoc
           suffix('.Transform2TPed.Success'),                       # Input suffix      = .Transform2TPed.Success
           '.ReplaceMissingPhenoasNA.Success')                      # Output suffix     = .ReplaceMissingPhenoasNA.Success
           
def replace_missingpheno_as_na(inputs, outputs):
    """
    Generate new .fam that replaces missing phenotypes as NA. The new .fam will be used to
    create a .phe file for emmax association testing.
    """
    
    
    flagFile = outputs
    print ("Generate new .fam that replaces missing phenotypes as NA. \n" 
    "The new .fam will be used to create a .phe file for emmax association testing")
    runStageCheck('replace_missingpheno_as_na', flagFile, qced_plink_bfiles)
    print "Task 3.1 completed successfully \n"
    
    
    
#Task3.2: qced_plink_missingpheno_as_NA.fam -> qced_plink.phe

@transform(replace_missingpheno_as_na,                              # Input prefix      = adv_assoc
           suffix('.ReplaceMissingPhenoasNA.Success'),              # Input suffix      = .ReplaceMissingPhenoasNA.Success
           add_inputs("qced_plink_missingpheno_as_NA.fam"),         # add_inputs        = qced_plink_missingpheno_as_NA.fam
           '.MakePheno.Success')                                    # Output suffix     = .MakePheno.Success
           
def make_pheno_for_emmax(inputs, outputs):
    """
    Make the phenotype file for qced_plink.{fam, bim, bed} in preparation for Emmax
    association testing.
    """
    
    qced_plink_fam_miss_as_NA = inputs[1]
    
    flagFile = outputs
    print ("Make the phenotype file for qced_plink.{fam, bim, bed} in preparation for Emmax \n"
    "association testing")
    runStageCheck('make_pheno_for_emmax', flagFile, qced_plink_fam_miss_as_NA)
    print "Task 3.2 completed successfully \n"

    
#Task3.3: qced_plink.tped -> qced_plink.aBN.kinf

@transform(make_pheno_for_emmax,                                    # Input prefix      = adv_assoc
           suffix('.MakePheno.Success'),                            # Input suffix      = .MakePheno.Success
           '.CreateKinshipMatrix.Success')                          # Output suffix     = .CreateKinshipMatrix.Success
           
def create_kinship_matrix(inputs, outputs):
    """
    Create a kinship matrix in preparation for Emmax association testing.
    """
    
    
    flagFile = outputs
    print "Creating a kinship matrix in preparation for Emmax association testing"
    runStageCheck('create_kinship_matrix', flagFile)
    print "Task 3.3 completed successfully \n"
    
    
#Task3.4: qced_plink.{tped, tfam} + qced_plink.aBN.kinf + qced_plink.phe  ->  qced_plink_emmax.ps

@transform(create_kinship_matrix,                                                        # Input prefix      = adv_assoc
           suffix('.CreateKinshipMatrix.Success'),                                       # Input suffix      = .CreateKinshipMatrix.Success
           add_inputs("qced_plink.aBN.kinf", "qced_plink.phe"),                          # add_inputs        = qced_plink.aBN.kinf and qced_plink.phe
           '.DoEmmaxTesting.Success')                                                    # Output suffix     = .DoEmmaxTesting.Success
           
def do_emmax_assoc_testing(inputs, outputs):
    """
    Use the phenotype, tped/tfam files, and kinship matrix files to run Emmax.
    """
    
    emmax_kin_matrix, emmax_pheno = inputs[1:]
    
    flagFile = outputs
    print "Using the phenotype, tped/tfam files, and kinship matrix files to run Emmax"
    runStageCheck('do_emmax_assoc_testing', flagFile, emmax_pheno, emmax_kin_matrix)
    print "Task 3.4 completed successfully \n"



#Task3.5: qced_smx_emmax.ps -> qced_smx_emmax_sorted.ps

@transform(do_emmax_assoc_testing,                                                       # Input prefix      = adv_assoc
           suffix('.DoEmmaxTesting.Success'),                                            # Input suffix      = .DoEmmaxTesting.Success
           add_inputs("qced_plink_emmax.ps"),                                            # add_inputs        = qced_smx_emmax_sorted.ps
           '.SortEmmaxResults.Success')                                                  # Output suffix     = .SortEmmaxResults.Success
           
def sort_emmax_results(inputs, outputs):
    """
    Sort Emmax association testing results.
    """
    
    emmax_assoc_results = inputs[1]
    
    flagFile = outputs
    print "Sorting Emmax association testing results"
    runStageCheck('sort_emmax_results', flagFile, emmax_assoc_results)
    print "Task 3.5 completed successfully \n"







