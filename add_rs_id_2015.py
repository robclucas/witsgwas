
input_1 = open("celfiles_pre_add_rsid.map")
output_1 = open("celfiles.map", "w")

#read through celfiles_pre_add_rsid.map line by line and assign each line to the list the_data
#such that each line becomes an element in the list the_data
the_data = input_1.readlines()

for line in the_data:
    new_line =  line.split("\t") #break up each line by tabs
    chrm = new_line[0]
    rs_id = new_line[1]
    number = new_line[2]
    position = new_line[3]
  #  allele = new_line[4]
    if rs_id == "---":
        #print line.strip()
        new_rs_id = "rs" + chrm + "_" + position.strip()
        output_1.write(chrm + "\t" + new_rs_id + "\t" + number + "\t" + position)
        #print (chrm + "\t" + new_rs_id + "\t" + number + "\t" + position + "\t" + allele)
    else:
         output_1.write(line)

input_1.close()
output_1.close()
