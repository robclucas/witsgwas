#!/bin/env python

""" pipeline_qcplink_tasks15-20of20.py 

    -pipeline for Sample and SNP qc using plink (version 1.9).
=============================================================================
"""

# system imports
import sys        # will use to exit sys if no input files are detected
import os		  # for changing directories
import datetime   # for adding timestamps to directories
import subprocess # for executing shell command, can be used instead of os.system()


# rubra and ruffus imports
from ruffus import *
from rubra.utils import pipeline_options
from rubra.utils import (runStageCheck, mkLogFile, mkDir, mkForceLink)


# user defined module imports
import Filemanager as FM

import WitsgwasSoftware as SW

import WitsgwasScripts as SC




# Shorthand access to options defined in pipeline_qcplink_config.py
#==========================================

working_files = pipeline_options.working_files
preselected_cutoff = pipeline_options.preselected_cutoff
logDir = pipeline_options.pipeline['logDir']



# Data setup process and input organisation
#==========================================

# assign the current project directory
# note: The pipeline will use this dir. for output and intermediate files.
SC.CURRENT_PROJECT_DIR = working_files['current_dir'] 
print "Current project directory %s" % SC.CURRENT_PROJECT_DIR


global witsGWAS_SCRIPTS_ROOT_DIR
witsGWAS_SCRIPTS_ROOT_DIR = "/opt/exp_soft/bioinf/witsGWAS/"


# cd into the current project dir.
os.chdir(SC.CURRENT_PROJECT_DIR)

# Check current working directory.
curr_work_dir = os.getcwd()
print "Current working directory %s" % curr_work_dir



# Paths to working files and intermediate result files
#==========================================

# path to plink binary files resulting from sample QC
sample_qced_plink_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "clean_inds_qcplink")

# path to plink binary files resulting from SNP QC
SNP_qced_plink_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "clean_qcplink")

# path to X chr SNPs i.e. xsnps.bim
x_SNPs_bim = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "xsnps.bim")


# path to plots generated during plink QC
qcplink_plots = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR, "qcplink_plots") + '/')

# path to plink binary files resulting from PLINK QC
fully_qced_plink_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "qced_plink")

# path to plink binary files resulting from prunning PLINK QCed files
qced_plink_pruned_bfiles = (os.path.join(witsGWAS_SCRIPTS_ROOT_DIR, SC.CURRENT_PROJECT_DIR) + "qced_plink_pruned")



# Preselected user cutoffs
#==========================================

# SNP differential missingness cutoff
cut_diff_miss = preselected_cutoff['differential_missingness_cutoff']

# SNP Hardy Weinburg deviation cutoff
cut_hwe = preselected_cutoff['hwe_cutoff']

# SNP minor allele frequency cutoff
cut_maf = preselected_cutoff['maf_cutoff']

# SNP missingness cutoff
cut_geno = preselected_cutoff['geno_cutoff']




# Print project information
#==========================================

print "Starting qcplink tasks15-20of20 for project: %s " % working_files['projectname']
print
print "Intermediate files and output will be stored in %s" % SC.CURRENT_PROJECT_DIR
print "Log dir is %s" % logDir
print "Project author is %s" % working_files['projectauthor'] 
print



#Task15: clean_inds_qcplink_test_missing.missing -> fail_diffmiss_qcplink.txt
    
@transform("qcplink.DiffMissplot.Success",                                                # Input             = qcplink
           suffix('.DiffMissplot.Success'),                                               # Input suffix      = .DiffMissplot.Success
           '.FailDiffMiss.Success')                                                       # Output suffix     = .FailDiffMiss.Success
           
def find_SNPs_extreme_diff_miss(inputs, outputs):
    """
    Select SNPs showing extreme differential missingness.
    """
        
    flagFile = outputs
    print "Based on a preselected cutoff,selecting SNPs showing extreme differential missingness."
    print "cutoffs should have been decided by looking at diffmiss_plot.pdf"
    runStageCheck('find_SNPs_extreme_diff_miss', flagFile, cut_diff_miss)
    print "Task 15 completed successfully \n"
    
    
#Task16: sample_qced_plink_bfiles -> clean_inds_qcplink_hwe.hwe

@transform(find_SNPs_extreme_diff_miss,                                                   # Input             = qcplink
           suffix('.FailDiffMiss.Success'),                                               # Input suffix      = .FailDiffMiss.Success
           '.FindHWEdeviations.Success')                                                  # Output suffix     = .FindHWEdeviations.Success
           
def find_SNPs_extreme_HWE_deviations(inputs, outputs):
    """
    Identify SNPs with extreme HWE deviations.
    """
    
    flagFile = outputs
    print "Identifying SNPs with extreme HWE deviations."
    runStageCheck('find_SNPs_extreme_HWE_deviations', flagFile, sample_qced_plink_bfiles)
    print "Task 16 completed successfully \n"
    
    
#Task17: clean_inds_qcplink_hwe.hwe -> clean_inds_qcplink_hweu.hwe

@transform(find_SNPs_extreme_HWE_deviations,                                              # Input             = qcplink
           suffix('.FindHWEdeviations.Success'),                                          # Input suffix      = .FindHWEdeviations.Success
           '.FindUnaffectedForHWEplot.Success')                                           # Output suffix     = .FindUnaffectedForHWEplot.Success
           
def find_unaffected_for_HWE_plot(inputs, outputs):
    """
    Select unaffected only from clean_inds_qcplink_hwe.hwe for HWE Plot.
    """
    
    flagFile = outputs
    print "Selecting unaffected only from clean_inds_qcplink_hwe.hwe for HWE Plot."
    runStageCheck('find_unaffected_for_HWE_plot', flagFile)
    print "Task 17 completed successfully \n"
    
    
#Task18: clean_inds_qcplink_hweu.hwe -> hwe_plot.pdf
    
@transform(find_unaffected_for_HWE_plot,                                                  # Input             = qcplink
           suffix('.FindUnaffectedForHWEplot.Success'),                                   # Input suffix      = .FindUnaffectedForHWEplot.Success
           '.hweplot.Success')                                                            # Output suffix     = .hweplot.Success
           
def generate_hwe_plot(inputs, outputs):
    """
    Plot the distribution of HWE P-values(in controls) using the script hwe_plot_qcplink.R.
    """
        
    flagFile = outputs
    print "Plotting the distribution of HWE P-values(in controls) using the script hwe_plot_qcplink.R"
    print "This plot should be used to decide an HWE P-value cutoff"
    runStageCheck('generate_hwe_plot', flagFile)
    print "Task 18 completed successfully \n"
    
    
#Task19: sample_qced_plink_bfiles and fail_diffmiss_qcplink.txt  -> clean_qcplink.{fam, bim, bed}

@transform(generate_hwe_plot,                                                             # Input             = qcplink
           suffix('.hweplot.Success'),                                                    # Input suffix      = .hweplot.Success
           add_inputs('fail_diffmiss_qcplink.txt'),                                       # additional Inputs = fail_diffmiss_qcplink.txt
           '.RemoveSNPsFailingQc.Success')                                                # Output suffix     = .RemoveSNPsFailingQc.Success
           
def remove_SNPs_failing_Qc(inputs, outputs):
    """
    Remove SNPs failing QC.
    """
    
    fail_diffmiss = inputs[1]
    
    flagFile = outputs
    print "Removing SNPs failing QC"
    runStageCheck('remove_SNPs_failing_Qc', flagFile, sample_qced_plink_bfiles, cut_maf, cut_geno, fail_diffmiss, cut_hwe)
    print "Task 19 completed successfully \n"
    
    
#Task20.0: SNP_qced_plink_bfiles  -> xsnps.{fam, bim, bed}

@transform(remove_SNPs_failing_Qc,                                                        # Input             = qcplink
           suffix('.RemoveSNPsFailingQc.Success'),                                        # Input suffix      = .RemoveSNPsFailingQc.Success
           '.FindXchrSNPs.Success')                                                       # Output suffix     = .FindXchrSNPs.Success
           
def find_xchr_SNPs(inputs, outputs):
    """
    Write out X chr SNPs to xsnps.{fam, bim, bed}.
    """
    
    flagFile = outputs
    print "Writing out X chr SNPs to xsnps.{fam, bim, bed}"
    runStageCheck('find_xchr_SNPs', flagFile, SNP_qced_plink_bfiles)
    print "Task 20.0 completed successfully \n"
    
    
#Task20.1: SNP_qced_plink_bfiles and xsnps.bim -> qced_plink.{fam, bim, bed}

@transform(find_xchr_SNPs,                                                                # Input             = qcplink
           suffix('.FindXchrSNPs.Success'),                                               # Input suffix      = .FindXchrSNPs.Success
           '.RemoveXchrSNPs.Success')                                                     # Output suffix     = .RemoveXchrSNPs
           
def remove_xchr_SNPs(inputs, outputs):
    """
    Remove X chr SNPs.
    """
    
    flagFile = outputs
    print "Removing X chr SNPs"
    runStageCheck('remove_xchr_SNPs', flagFile, SNP_qced_plink_bfiles, x_SNPs_bim)
    print "Task 20.1 completed successfully \n"


#Task20.2: qced_plink.{fam, bim, bed} -> qced_plink_pruned.prune.in                       # Goal of the task

@transform(remove_xchr_SNPs,                                                              # Input prefix      = qcplink
           suffix('.RemoveXchrSNPs.Success'),                                             # Input suffix      = .RemoveXchrSNPs.Success
           '.QcedPlinkPruneIn.Success')                                                   # Output suffix     = .QcedPlinkPruneIn.Success

def prune_qced_plink_for_pca(inputs, outputs):                                            
    """
    Minimize computational complexity prior to carrying out PCA by pruning the SNPs; such that
    no pair of SNPs within a 50-SNP sliding window (advanced by 5 SNPs each time) has an
    r_squared value greater than 0.2
    """

    flagFile = outputs                                                                              
    print ("pruning the SNPs such that no pair of SNPs within a 50-SNP sliding window. \n" 
    "(advanced by 5 SNPs each time) has an r_squared value greater than 0.2")
    runStageCheck('prune_qced_plink_for_pca', flagFile, fully_qced_plink_bfiles)                             
    print "Task 20.2 completed successfully \n"
    

#Task20.3: qced_plink_pruned.prune.in -> qced_plink_pruned.{fam, bim, bed}                # Goal of the task

@transform(prune_qced_plink_for_pca,                                                      # Input prefix      = qcplink
           suffix('.QcedPlinkPruneIn.Success'),                                           # Input suffix      = .QcedPlinkPruneIn.Success
           add_inputs("qced_plink_pruned.prune.in"),                                      # add_inputs        = qced_plink_pruned.prune.in
           '.QcedPlinkPruned.Success')                                                    # Output suffix     = .QcedPlinkPruned.Success

def extract_prunein_qcedplink_for_pca(inputs, outputs):                                            
    """
    Generate a subset of SNPs that are in approximate LD (Linkage disequilibrium) with each other
    in preparation for running PCA
    """

    prune_in_snps = inputs[1]                                                             # assigning qced_plink_pruned.prune.in to prune_in_snps

    flagFile = outputs                                                                              
    print ("Generating a subset of SNPs that are in approximate LD (Linkage disequilibrium) \n"
    " with each other in preparation for running PCA")
    runStageCheck('extract_prunein_qcedplink_for_pca', flagFile, fully_qced_plink_bfiles, prune_in_snps)                             
    print "Task 20.3 completed successfully \n"
    

#Task20.4: qced_plink_pruned.{fam, bim, bed} -> qced_plink_pruned.pca.evec                # Goal of the task

@transform(extract_prunein_qcedplink_for_pca,                                             # Input prefix      = qcplink
           suffix('.QcedPlinkPruned.Success'),                                            # Input suffix      = .QcedPlinkPruned.Success
           '.QcedPlinkPrunedPCA.Success')                                                 # Output suffix     = .QcedPlinkPrunedPCA.Success

def do_pca_on_pruned_qcedplink(inputs, outputs):

    """
    Run PCA on qced_plink_pruned.{fam, bim, bed} to determine whether the cases and 
    controls are from similar populations.
    """

    flagFile = outputs 
    print ("Run PCA on qced_plink_pruned.{fam, bim, bed} to determine whether the cases and \n"
    " controls are from similar populations")
    runStageCheck('do_pca_on_pruned_qcedplink', flagFile)                             
    print "Task 20.4 completed successfully \n"
    
#Task20.5: qced_plink_pruned.eval -> qced_plink_pruned.tw                                 # Goal of the task

@transform(do_pca_on_pruned_qcedplink,                                                    # Input prefix      = qcplink                                                             
           suffix('.QcedPlinkPrunedPCA.Success'),                                         # Input suffix      = .QcedPlinkPrunedPCA.Success
           add_inputs("qced_plink_pruned.eval"),                                          # add inputs        =  qced_plink_pruned.eval                                          
           '.QcedPlinkPCATwstats.Success')                                                # Output suffix     = .QcedPlinkPCATwstats.Success
           
def do_twstats_on_pca_results(inputs, outputs):

    """                                                                                                                                                                             
    Which eigenvectors are significant?.
    Used twstats to assess if there is significant structure in each PC.                                                                                                  
    """
    
    qcedplink_pca_eval = inputs[1]

    flagFile = outputs
    print "Using twstats to assess if there is significant structure in each PC"
    runStageCheck('do_twstats_on_pca_results', flagFile, qcedplink_pca_eval)
    print "Task 20.5 completed successfully \n"
    
